
The backend-api is a java jar-file that can be used to access and modify the domain model.

Use it, the only requirement is to have a property available with the name

.. code:: bash
	
	<#noparse>${backend.base.url}</#noparse>
	
	