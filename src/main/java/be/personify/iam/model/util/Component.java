package be.personify.iam.model.util;

public enum Component {
	
	AUTHENTICATION,
	AUTHORIZATION,
	GATEWAY,
	PROVISIONING,
	REPORTING,
	VAULT,
	WORKFLOW,
	GENERAL
	
}
