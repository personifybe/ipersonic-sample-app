function delete(){
        POD_PRESENT=`kubectl -n personify get pods | grep $1 | wc -l`
        if [ "$POD_PRESENT" != "0" ] ; then
            kubectl delete --ignore-not-found=true -n personify service $1 | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
            kubectl delete --ignore-not-found=true -n personify deployment $1 | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
            PERCENTAGE=$((PERCENTAGE+1))
        fi
}


#auth-login
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'authentication-login'>
delete ${info.projectName}-${module.name}
		</#if>
</#list>
#frontends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'ui' || module['type'] == 'documentation'>
delete ${info.projectName}-${module.name}
		</#if>
</#list>
#modules
<#list projectConfiguration['modules'] as module>
		<#if module['name']?starts_with("module")>
delete ${info.projectName}-${module.name}
		</#if>
</#list>
#backends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'backend' >
delete ${info.projectName}-${module.name}
		</#if>
</#list>

#public
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'public'>
delete ${info.projectName}-${module.name}
		</#if>
</#list>







