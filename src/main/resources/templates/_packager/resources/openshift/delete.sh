oc login -u developer -p developer


#auth-login
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'authentication-login'>
oc delete svc/${info.projectName}-${module.name}
oc delete dc/${info.projectName}-${module.name}
		</#if>
</#list>
#frontends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'ui' || module['type'] == 'documentation' >
oc delete svc/${info.projectName}-${module.name}
oc delete dc/${info.projectName}-${module.name}
		</#if>
</#list>
#modules
<#list projectConfiguration['modules'] as module>
		<#if module['name']?starts_with("module")>
oc delete svc/${info.projectName}-${module.name}
oc delete dc/${info.projectName}-${module.name}
		</#if>
</#list>
#backends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'backend' >
oc delete svc/${info.projectName}-${module.name}
oc delete dc/${info.projectName}-${module.name}
		</#if>
</#list>

#public
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'public'>
oc delete svc/${info.projectName}-${module.name}
oc delete dc/${info.projectName}-${module.name}
		</#if>
</#list>

oc delete route personify.be
oc delete route ipersonic.be





