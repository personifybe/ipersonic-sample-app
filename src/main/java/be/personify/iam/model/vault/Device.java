package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;
import be.personify.iam.model.util.location.Location;

@Entity
@MetaInfo( group="vault", frontendGroup="Identity", name="Device", description="A device", 
		showInMenu=true, 
		sortOrderInGroup=4,
		iconClass="microchip",
		workflowEnabled=false,
		number=38,
		isConcept = true)
public class Device extends Concept  implements Serializable{

	
	private static final long serialVersionUID = -6222953408587470644L;
	
	@MetaInfo(name="name",description="the name of the device",sampleValue="Nexus IV")
	private String name;
	
	
	@MetaInfo(name="mobile",description="indicating if the device is mobile or portable")
	private boolean mobile;
	
	@MetaInfo(name="device identifier",description="identifier uniquely idnetifying the device ( MAC )", sampleValue = "00:25:96:FF:FE:12:34:56", searchable=false, showInSearchResultGrid=false)
	private String deviceIdentifier;
	
	@Enumerated(EnumType.STRING)
	@MetaInfo( name="device type", description="The type of the device ( phone, laptop, tablet, wearable, .... )", searchable=false, showInSearchResultGrid=true)
	private DeviceType deviceType;
	
	@OneToOne
	@MetaInfo( name="location", description="the location of this device", searchable=false, showInSearchResultGrid=false)
	private Location location;
	
	@OneToOne
	@MetaInfo( name="identity", description="the identity belonging to this device", searchable=true, showInSearchResultGrid=true, customRenderer = "autoCompleteTextField|search_fields=lastName|display_fields=lastName,firstName,code")
	private Identity identity;

	public Device() {
		this.setConceptType(ConceptType.Device);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public boolean isMobile() {
		return mobile;
	}

	public void setMobile(boolean mobile) {
		this.mobile = mobile;
	}

	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}

	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deviceIdentifier == null) ? 0 : deviceIdentifier.hashCode());
		result = prime * result + ((deviceType == null) ? 0 : deviceType.hashCode());
		result = prime * result + ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + (mobile ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Device other = (Device) obj;
		if (deviceIdentifier == null) {
			if (other.deviceIdentifier != null)
				return false;
		} else if (!deviceIdentifier.equals(other.deviceIdentifier))
			return false;
		if (deviceType != other.deviceType)
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (mobile != other.mobile)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
	
	
	
	
}
