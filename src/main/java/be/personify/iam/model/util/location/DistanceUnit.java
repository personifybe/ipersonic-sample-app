package be.personify.iam.model.util.location;

public enum DistanceUnit {
	METERS, KILOMETERS, MILES;
}
