#database
oc create -f _03_app_database.yaml

echo "sleeping 20 seconds, waiting for database to come up"
sleep 20

#backends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'backend' >
oc create -f ../../../../../${module.name}/create-app.yaml
		</#if>
</#list>

echo "sleeping 40 seconds, waiting for backends to come up"
sleep 40

#modules
<#list projectConfiguration['modules'] as module>
		<#if module['name']?starts_with("module")>
oc create -f ../../../../../${module.name}/create-app.yaml
echo "waiting [${(module['configuration']['startup-time'])!"10"}] seconds, giving module [${module.name}] time to initialize"
sleep ${(module['configuration']['startup-time'])!"10"}
		</#if>
</#list>

echo "sleeping 60 seconds, waiting for modules to come up"
sleep 60

#public
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'public'>
oc create -f ../../../../../${module.name}/create-app.yaml
		</#if>
</#list>

echo "sleeping 10 seconds, waiting for public site to come up"
sleep 10

#auth login
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'authentication-login'>
oc create -f ../../../../../${module.name}/create-app.yaml
		</#if>
</#list>

echo "sleeping 10 seconds, waiting for authentication site to come up"
sleep 10

#frontends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'ui' || module['type'] == 'documentation' >
oc create -f ../../../../../${module.name}/create-app.yaml
		</#if>
</#list>

oc create -f _99_route_personify_1.yaml
oc create -f _99_route_personify_2.yaml
oc create -f _99_route_ipersonic_3.yaml
oc create -f _99_route_ipersonic_4.yaml
oc create -f _99_route_mogo_5.yaml

