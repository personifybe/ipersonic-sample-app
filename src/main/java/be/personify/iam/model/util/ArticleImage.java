package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Governance", name="article", description="A article", iconClass="sitemap", 
				sortOrderInGroup=1, showInMenu=false,
				isConcept=false, number=1,
				workflowEnabled = false,
				afterCreateGoToLink = "article",
				afterDeleteGoToLink = "article",
				afterUpdateGoToLink = "article"
)
@Table(name="article_image")
public class ArticleImage extends Persisted implements Serializable {
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(showInSearchResultGrid=false , name="id", description="the id of the article image")
	private long id;
	
	@Column(nullable=false)
	@MetaInfo(name="name",description="the name of the article image")
	private String name;
	
	@Column(nullable=false)
	@MetaInfo(name="description",description="the description of the article image", showInSearchResultGrid = false)
	private String description;
	
	@MetaInfo(name="contentType",description="the contentType of the article image", showInSearchResultGrid = true, searchable = false)
	private String contentType;
	
	@MetaInfo(name="content",description="the content of the article image", showInSearchResultGrid = false, searchable = false)
	@Lob
	private byte[] content;
	
	@MetaInfo(name="article",description="the article of the article image", showInSearchResultGrid = false, searchable = false)
	@ManyToOne( targetEntity=Article.class)
	private Article article;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((article == null) ? 0 : article.hashCode());
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + Arrays.hashCode(content);
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArticleImage other = (ArticleImage) obj;
		if (article == null) {
			if (other.article != null)
				return false;
		} else if (!article.equals(other.article))
			return false;
		if (contentType == null) {
			if (other.contentType != null)
				return false;
		} else if (!contentType.equals(other.contentType))
			return false;
		if (!Arrays.equals(content, other.content))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	
	
	
	

}
