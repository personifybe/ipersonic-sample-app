#!/bin/bash
<#noparse>

APP_NAME="personify-scim-server"
BASE_DIR=".."
JAR_FILE="personify-scim-server-1.2.7.RELEASE.jar"
PID="/var/tmp/scim-server.pid"
URL=http:/localhost:8765

source ./functions.sh


case "$1" in
  "start" )
    if [ ! -f ${PID} ]; then
      echo "Starting ${APP_NAME} ....."
      $_java -XX:+UseParallelGC -XX:MinHeapFreeRatio=5 -XX:MaxHeapFreeRatio=10 -XX:GCTimeRatio=4 -Xms128m -Xmx256m -Dlog4j.configurationFile=/home/personify/scim/log4j2.xml -Dserver.port=8765 -jar ${JAR_FILE} &
      echo $! > ${PID}
    else
      echo "${APP_NAME} is already running."
    fi
    ;;
  "stop" )
  	if [ ! -f ${PID} ]; then
  		echo "${PID} not found."
  	else	
    	echo "Stopping ${APP_NAME} ....."
    	kill `cat ${PID}`
	    RETVAL=$?
	    if test $RETVAL -eq 0 ; then
	        echo "succesfully stopped"
	        rm -f ${PID}
	    fi
	    if test $RETVAL -eq 1 ; then
	        echo "could not stop, remove pid file anyway stopped"
	        rm -f ${PID}
	    fi
	fi
    ;;
  "status" )
    response=$(curl --write-out \\n%{http_code} --silent --output - ${URL}/system/health)
    echo scim-server- ${response}
    ;;
  *)
    echo "Usage: scim-server.sh  start|stop|status"
    exit 1
esac
</#noparse>