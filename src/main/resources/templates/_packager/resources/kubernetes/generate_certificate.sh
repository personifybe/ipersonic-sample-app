openssl genrsa -out personify_key.pem
openssl req -new -key personify_key.pem -out personify_cert.csr
openssl x509 -req -in personify_cert.csr -signkey personify_key.pem -out personify_cert.crt
openssl pkcs12 -export -in personify_cert.crt -name tomcat -inkey personify_key.pem -out keystore.p12