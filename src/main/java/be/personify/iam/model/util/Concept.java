package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import be.personify.iam.model.ConceptType;
import be.personify.iam.model.vault.Identity;
import be.personify.util.generator.MetaInfo;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Table(name="concept", indexes = {
		@Index(name = "idx_concept_state", columnList = "concept_state", unique=false),
		@Index(name = "idx_concept_type", columnList = "concept_type", unique=false)
})
@MetaInfo( group="vault", frontendGroup="Utility", name="Concept", description="A generic concept", iconClass = "comment-alt",number=26)
public class Concept extends Persisted implements Serializable {
	
	private static final long serialVersionUID = -8027232410259710799L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the concept", showInSearchResultGrid = false)
	private long id;
	
		
	@Enumerated(EnumType.STRING)
    @NotNull
    @Column(name="concept_type")
	//@JsonIgnore -> not here, deliberately!
	@MetaInfo(name="conceptType", description="the type of the concept")
	public ConceptType conceptType;
	
	@Enumerated(EnumType.STRING)
    @NotNull
    @Column(name="concept_state")
	//@JsonIgnore
	@MetaInfo(name="conceptState", description="the state of the concept")
	public ConceptState conceptState = ConceptState.INACTIVE;
	
	@Column(columnDefinition = "TEXT")
	@MetaInfo(name="conceptStateDetails", description="The conceptStateDetails, contains details about errors etc", showInSearchResultGrid = false, customRenderer = "text_area|rows:10", searchable = false)
	private String conceptStateDetails;
	
	@Column(columnDefinition="DATETIME")
	@Temporal(TemporalType.TIMESTAMP)
	@MetaInfo(name="lastRecertificationDate", description="the lastCertificationDate of the concept")
	private Date lastCertificationDate;
	
	@ManyToOne( targetEntity=Identity.class)
	@MetaInfo(searchable=false,showInSearchResultGrid=true, editable = false, name="lastRecertifier", description = "the lastRecertifier of this concept", customRenderer = "autoCompleteTextField|search_fields=lastName|display_fields=lastName,firstName,code")
	private Identity lastCertifier;
	 
	
	
	public Concept() {}
	
	public Concept( ConceptType t ) {
		this.conceptType = t;
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ConceptType getConceptType() {
		return conceptType;
	}

	public void setConceptType(ConceptType conceptType) {
		this.conceptType = conceptType;
	}

	
	public ConceptState getConceptState() {
		return conceptState;
	}

	public void setConceptState(ConceptState conceptState) {
		this.conceptState = conceptState;
	}

	
	public String getConceptStateDetails() {
		return conceptStateDetails;
	}

	public void setConceptStateDetails(String conceptStateDetails) {
		this.conceptStateDetails = conceptStateDetails;
	}
	
	public Date getLastCertificationDate() {
		return lastCertificationDate;
	}

	public void setLastCertificationDate(Date lastCertificationDate) {
		this.lastCertificationDate = lastCertificationDate;
	}

	public Identity getLastCertifier() {
		return lastCertifier;
	}

	public void setLastCertifier(Identity lastCertifier) {
		this.lastCertifier = lastCertifier;
	}

	@Override
	public String toString() {
		return "Concept [id=" + id + ", conceptType=" + conceptType + ", conceptState=" + conceptState + "]";
	}
	

}
