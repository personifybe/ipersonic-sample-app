#!/bin/bash
dockerMode="i"
if [ "$#" -eq  "0" ]
   then
     echo "No docker mode given ( first arg ), defaulting to -$dockerMode"
else
     dockerMode="$1"
     echo "Using docker mode -$dockerMode"
fi


docker run -$dockerMode --rm \
	-p 1389:1389 -p 1636:1636 \
	--name=personify-ldap \
	 personify/personify-ldap:${projectConfiguration.version}
