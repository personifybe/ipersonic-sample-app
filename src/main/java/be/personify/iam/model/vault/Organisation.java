package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;


@Entity
@MetaInfo(group="vault", frontendGroup = "Organisation", 
			name = "Organisation", 
			number=2, 
			description = "A organisation", 
			showInMenu = true, iconClass = "sitemap", 
			sortOrderInGroup = 1, workflowEnabled = false, 
			sortOrder = "ascending",
			sortProperty = "name",
			isConcept = true,
			identityDirection = "organisationAssignments",
			showDeleteButtonOnSearchResult = false,
			showEditButtonOnSearchResult = false,
			dashboard = true,
			dashboardField = "code")
@Table(name="organisation", indexes = {
			@Index(name = "idx_organisation_name", columnList = "name", unique=true),
			@Index(name = "idx_organisation_code", columnList = "code", unique=true)
		}
)
public class Organisation extends Concept implements Serializable {

	private static final long serialVersionUID = 7674004398321178066L;

	@MetaInfo( name="name", description="a name describing the organisation", required = true)
	private String name;

	@MetaInfo( name="code", description="a unique code for the organisation", required = true, showInSearchResultGridMobile = false)
	private String code;
	
	@MetaInfo( name="selfRequestableOrganisationAssignments", description="determines if the organisationAssignment is selfrequestable for the organisation", showInSearchResultGrid = false, searchable = false)
	@Column(name = "self_requestable_org_ass")
	private Boolean selfRequestableOrganisationAssignments = Boolean.FALSE;
	
	@MetaInfo( name="selfRequestableEntitlementAssignments", description="determines if the entitlementAssignment is selfrequestable for the organisation", showInSearchResultGrid = false, searchable = false)
	@Column(name = "self_requestable_ent_ass")
	private Boolean selfRequestableEntitlementAssignments = Boolean.FALSE;
	
	
	@OneToMany(mappedBy = "organisation", cascade = CascadeType.ALL)
	@ElementCollection(targetClass = OrganisationAssignment.class)
	@MetaInfo( name="organisationAssignments", description="defining the identities linked to the organisation. See organisationAssignment_", dashboardSubtypeDirection = "identity.identityType.code")
	private List<OrganisationAssignment> organisationAssignments;
	

	@OneToMany(mappedBy = "organisation", cascade = CascadeType.ALL)
	@ElementCollection(targetClass = OrganisationTypeAssignment.class)
	@MetaInfo( name="organisationTypeAssignments", description="the organisation types assigned to the organisation. See organisationTypeAssignment_", showDetailButtonOnSearchResult = false, showEditButtonOnSearchResult = false)
	private List<OrganisationTypeAssignment> organisationTypeAssignments;
	
	

	public Organisation() {
		this.conceptType = ConceptType.Organisation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<OrganisationTypeAssignment> getOrganisationTypeAssignments() {
		return organisationTypeAssignments;
	}

	public void setOrganisationTypeAssignments(List<OrganisationTypeAssignment> organisationTypeAssignments) {
		this.organisationTypeAssignments = organisationTypeAssignments;
	}

	public List<OrganisationAssignment> getOrganisationAssignments() {
		return organisationAssignments;
	}

	public void setOrganisationAssignments(List<OrganisationAssignment> organisationAssignments) {
		this.organisationAssignments = organisationAssignments;
	}

	public boolean isSelfRequestableOrganisationAssignments() {
		return selfRequestableOrganisationAssignments;
	}

	public void setSelfRequestableOrganisationAssignments(boolean selfRequestableOrganisationAssignments) {
		this.selfRequestableOrganisationAssignments = selfRequestableOrganisationAssignments;
	}

	public boolean isSelfRequestableEntitlementAssignments() {
		return selfRequestableEntitlementAssignments;
	}

	public void setSelfRequestableEntitlementAssignments(boolean selfRequestableEntitlementAssignments) {
		this.selfRequestableEntitlementAssignments = selfRequestableEntitlementAssignments;
	}

	@Override
	public String toString() {
		return "Organisation [name=" + name + ", code=" + code + ", selfRequestableOrganisationAssignments="
				+ selfRequestableOrganisationAssignments + ", selfRequestableEntitlementAssignments="
				+ selfRequestableEntitlementAssignments + ", organisationAssignments=" + organisationAssignments
				+ ", organisationTypeAssignments=" + organisationTypeAssignments + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((organisationAssignments == null) ? 0 : organisationAssignments.hashCode());
		result = prime * result + ((organisationTypeAssignments == null) ? 0 : organisationTypeAssignments.hashCode());
		result = prime * result + (selfRequestableEntitlementAssignments ? 1231 : 1237);
		result = prime * result + (selfRequestableOrganisationAssignments ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Organisation other = (Organisation) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (organisationAssignments == null) {
			if (other.organisationAssignments != null)
				return false;
		} else if (!organisationAssignments.equals(other.organisationAssignments))
			return false;
		if (organisationTypeAssignments == null) {
			if (other.organisationTypeAssignments != null)
				return false;
		} else if (!organisationTypeAssignments.equals(other.organisationTypeAssignments))
			return false;
		if (selfRequestableEntitlementAssignments != other.selfRequestableEntitlementAssignments)
			return false;
		if (selfRequestableOrganisationAssignments != other.selfRequestableOrganisationAssignments)
			return false;
		return true;
	}

	
	
	
	
	
	
	

}
