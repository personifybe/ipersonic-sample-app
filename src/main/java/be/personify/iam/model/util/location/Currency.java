package be.personify.iam.model.util.location;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

	

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="Currency", description="A currency", iconClass = "euro-sign", number=30)
public class Currency extends Persisted implements Serializable {

	public static final long serialVersionUID = -7370909171960266468L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private String name;
	
	private String code;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	
}
