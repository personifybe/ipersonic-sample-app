#title

## run from maven
mvn spring-boot:run

## run
java -jar target/${projectName}-${moduleName}.jar

## build the docker image
build the dockerfile : mvn install dockerfile:build