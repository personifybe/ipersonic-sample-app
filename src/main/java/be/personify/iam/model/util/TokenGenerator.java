package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyColumn;

import be.personify.util.generator.MetaInfo;


@Entity
@MetaInfo( group="vault", frontendGroup="Utility", name="Token generator", description="A token generator", 
	number=24,iconClass = "comment-alt")
public class TokenGenerator extends Persisted implements Serializable{

	
	private static final long serialVersionUID = -7073831510079699183L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the tokengenerator")
	private long id;
	
	@Enumerated( EnumType.STRING )
	@MetaInfo(name="tokenContext", description="the tokencontext of the tokengenerator ( mobile or email confirmation )")
	private TokenContext tokenContext;
	
	@MetaInfo(name="className", description="The class implementation of the tokengenerator")
	private String className;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name="generator_configuration", joinColumns=@JoinColumn(name="id"))
	@MapKeyColumn (name="configuration_id")
	@Column(name="value")
	@MetaInfo(name="configuration", description="The configuration of the tokengenerator")
	private Map<String,String> configuration;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TokenContext getTokenContext() {
		return tokenContext;
	}

	public void setTokenContext(TokenContext tokenContext) {
		this.tokenContext = tokenContext;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}
	
	
	
}
