package be.personify.iam.model.util;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import be.personify.util.generator.MetaInfo;


@Entity
@MetaInfo( group="vault", frontendGroup="Utility", name="Server status", description="the status of a server",
					number=21,iconClass = "comment-alt")
public class ServerStatus extends Persisted  implements Serializable{

	private static final long serialVersionUID = 1797157339855588526L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the server status")
	private long id;


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
