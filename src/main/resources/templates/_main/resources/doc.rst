===============================
${projectName}
===============================

.. contents:: Table of Contents
   :depth: 4

Installation
==============================

This product is either deployed as a stand-alone version requiring manual installation, either a a set of OpenShift compliant docker images.

If deployed on openshift, the docker images can be located here : https://hub.docker.com/u/personify
First the database has to be created, then the backend, the modules and the frontends.

When installing manually, the product is compatible with the following JAVA versions :

+--------------------------------+--------------------------------+-----+
| name                           | version                        |     |
+================================+================================+=====+
| Oracle JDK                     | 8u221                          | yes |
+--------------------------------+--------------------------------+-----+
| OpenJDK                        | 1.8.0_172                      | yes |
+--------------------------------+--------------------------------+-----+


For each module, the installation procedures have to be followed.

When extracting the received package, the content will look like this ( depending on the available modules ).

.. figure:: ./img/tree.png
   :scale: 100 %
   :width: 600
   :alt: no diagram present
   
   
 
- bin : containing start scripts ( global and for each module
- lib : the binaries
- conf : you can override the config here
- doc : containing documentation
- extensions : extra jar files ( db-drivers, ... )

Be careful to read all the necessary prerequisites for installing the back-ends.
Once everything is properly configured , following script can be executed :

.. code:: bash

	./personify.sh start|stop|status




Backends
------------------------------------

For each separate backend module, a individual script can be used to start and stop the backend application.

.. code:: bash

	./backend-vault.sh start|stop|status


<#list projectConfiguration['modules'] as module>
	<#if module['type'] == 'backend' >
${(module['description'])!"noname"}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. include:: ../../../../${(module['name'])!"noname"}/src/main/resources/doc/installation.rst
	</#if>
</#list>


Frontends
------------------------------------
All the user interfaces are packaged as separate WAR files and located in the lib folder.
Via these UI-applications, all the modules can be configured. 

They can be copied to a J2EE compliant servlet engine ( Apache Tomcat, Jetty ) using the script :

.. code:: bash

	./copy_war_to_tomcat.sh 
	'../lib/personify-ui-authentication-admin-1.0.0-SNAPSHOT.war' -> '/var/lib/tomcat8/webapps/ui-authentication-admin.war'
	'../lib/personify-ui-authentication-login-1.0.0-SNAPSHOT.war' -> '/var/lib/tomcat8/webapps/ui-authentication-login.war'
	'../lib/personify-ui-gateway-admin-1.0.0-SNAPSHOT.war' -> '/var/lib/tomcat8/webapps/ui-gateway-admin.war'
	'../lib/personify-ui-vault-admin-1.0.0-SNAPSHOT.war' -> '/var/lib/tomcat8/webapps/ui-vault-admin.war'
	'../lib/personify-ui-provisioning-admin-1.0.0-SNAPSHOT.war' -> '/var/lib/tomcat8/webapps/ui-provisioning-admin.war'

For this to work, you should first set the TOMCAT_HOME variable to the correct directory :

.. code:: bash

	export TOMCAT_HOME=/usr/liv/apache-tomcat-xxxx

You can also copy them manually to your preferred servlet-container.

The user interface will be then visible at e.g. :

http://localhost:8080/ui-provisioning-admin/

being the context the name of the war file.



Extra modules
------------------------------------
Extra modules contain supplemental logic like schedulers, provisioning and system setup.

<#list projectConfiguration['modules'] as module>
	<#if module['type'] != 'backend' >
	<#if module['type'] != '_packager' >
	<#if module['name'] != '_main' >
	<#if module['type'] != 'client-api' >
	<#if module['type'] != 'ui' >
${(module['description'])!"noname"}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. include:: ../../../../${(module['name'])!"noname"}/src/main/resources/doc/installation.rst

	</#if>
	</#if>
	</#if>
	</#if>
	</#if>
</#list>



Documentation
==============================



Backends
------------------------------------
<#list projectConfiguration['modules'] as module>
	<#if module['type'] == 'backend' >
${(module['description'])!"noname"}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../../${(module['name'])!"noname"}/src/main/resources/doc/main.rst

	</#if>
</#list>



Extra modules
------------------------------------
<#list projectConfiguration['modules'] as module>
	<#if module['type'] != 'backend' >
	<#if module['type'] != '_packager' >
	<#if module['name'] != '_main' >
	<#if module['type'] != 'client-api' >
	<#if module['type'] != 'ui' >
${(module['description'])!"noname"}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


.. include:: ../../../../${(module['name'])!"noname"}/src/main/resources/doc/main.rst

	</#if>
	</#if>
	</#if>
	</#if>
	</#if>
</#list>






----------------------------------------------


:Authors: 
    Wouter Van der Beken

:Version: ${projectConfiguration['version']} generated on  ${generationDate} 

