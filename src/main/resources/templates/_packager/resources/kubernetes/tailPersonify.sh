if [[ -z $1 ]]; then
    echo "give the module and lines as argument e.g : ./tailPersonify.sh personify-module-vault 1000"
    exit -1 
fi
lines=$2
if [[ -z $2 ]]; then
	lines=1000
fi
kubectl logs -f -l app=personify-$1 --tail=$lines -n personify