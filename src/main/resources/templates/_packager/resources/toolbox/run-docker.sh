#!/bin/bash
dockerMode="i"
if [ "$#" -eq  "0" ]
   then
     echo "No docker mode given ( first arg ), defaulting to -$dockerMode"
else
     dockerMode="$1"
     echo "Using docker mode -$dockerMode"
fi

docker run -$dockerMode --rm \
	-p 4444:25 -v /tmp/exim:/var/spool/exim4 \
	--name=personify-toolbox \
	 personify/personify-toolbox:${projectConfiguration.version}
