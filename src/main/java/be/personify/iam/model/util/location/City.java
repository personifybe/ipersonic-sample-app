package be.personify.iam.model.util.location;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonProperty;

import be.personify.iam.model.util.Persisted;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="City", description="A city", iconClass = "map-signs", number=28, isConcept = false)
public class City extends Persisted implements Serializable{
	
	public static final long serialVersionUID = 7781178342387600890L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private String name;
	
	@ManyToOne
	@JoinColumn(name="country_id")
	@JsonProperty
	private Country country;
	
	@ManyToOne
	@JoinColumn(name="province_id")
	@JsonProperty
	@MetaInfo(searchable = false)
	private Province province;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="location_id")
	@MetaInfo(searchable = false)
	private Location location;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}
	
	
	
	

}
