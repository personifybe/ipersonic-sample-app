oc login -u developer -p developer
token=`oc whoami -t`
docker login -u developer -p $token 172.30.1.1:5000

docker tag ${info.projectName}/${info.projectName}-${moduleName}:${projectConfiguration.version} 172.30.1.1:5000/${projectConfiguration.dockerspace!"personify"}/${info.projectName}-${moduleName}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${projectConfiguration.dockerspace!"personify"}/${info.projectName}-${moduleName}:${projectConfiguration.version}