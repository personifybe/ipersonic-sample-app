package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="Identity", name="Consent", description="A consent", 
		showInMenu=true, 
		iconClass="thumbs-up", 
		sortOrderInGroup=5, 
		workflowEnabled=false,
		isConcept = false,
		number=5,
		addable=true,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false)
@Table(name="consent", indexes = {
		@Index(name = "idx_consent_uniq", columnList = "identity_id,processing_purpose_id", unique=true)
})
public class Consent extends Persisted implements Serializable{
	
	private static final long serialVersionUID = -360908527068463645L;
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the task", showInSearchResultGrid = false)
	private long id;


	@MetaInfo( name="identity", description="The identity giving the consent", searchable=true, customRenderer = "autoCompleteTextField|search_fields=lastName|display_fields=lastName,firstName,code")
	@ManyToOne
	private Identity identity;
	
	@MetaInfo( name="processingPurpose", description="The processingPurpose of the consent", searchable=true )
	@ManyToOne
	private ProcessingPurpose processingPurpose;
	
	@MetaInfo( name="choice", description="choice indicating OPTIN or OPTOUT", searchable=true, showInSearchResultGrid=true, viewable = true, sortOrderInGroup=1)
	@Enumerated(EnumType.STRING)
	private ConsentChoice choice;

	@MetaInfo( name="startDate", description="The startDate of the consent", searchable=true )
	private Date startDate;
	
	@MetaInfo( name="endDate", description="The endDate of the consent", searchable=true )
	private Date endDate;
	
	@Column(nullable=false)
	@MetaInfo(name="dataParts",description="the dataParts of the consent", searchable = false, showInSearchResultGrid = false)
	@ElementCollection(fetch = FetchType.EAGER)
	private List<String> dataParts = new ArrayList<>();
	
	

	public Consent() {
		super();
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public Identity getIdentity() {
		return identity;
	}



	public void setIdentity(Identity identity) {
		this.identity = identity;
	}



	public ProcessingPurpose getProcessingPurpose() {
		return processingPurpose;
	}



	public void setProcessingPurpose(ProcessingPurpose processingPurpose) {
		this.processingPurpose = processingPurpose;
	}



	public ConsentChoice getChoice() {
		return choice;
	}



	public void setChoice(ConsentChoice choice) {
		this.choice = choice;
	}



	public Date getStartDate() {
		return startDate;
	}



	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}



	public Date getEndDate() {
		return endDate;
	}



	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}



	public List<String> getDataParts() {
		return dataParts;
	}



	public void setDataParts(List<String> dataParts) {
		this.dataParts = dataParts;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((choice == null) ? 0 : choice.hashCode());
		result = prime * result + ((dataParts == null) ? 0 : dataParts.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((processingPurpose == null) ? 0 : processingPurpose.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Consent other = (Consent) obj;
		if (choice != other.choice)
			return false;
		if (dataParts == null) {
			if (other.dataParts != null)
				return false;
		} else if (!dataParts.equals(other.dataParts))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (id != other.id)
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (processingPurpose == null) {
			if (other.processingPurpose != null)
				return false;
		} else if (!processingPurpose.equals(other.processingPurpose))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}


	

	
	
	

}
