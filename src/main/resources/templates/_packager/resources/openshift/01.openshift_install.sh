oc login -u developer -p developer
oc new-project personify --display-name="Personify" \--description="Personify Project"

token=`oc whoami -t`

docker login -u developer -p $token 172.30.1.1:5000

./push.sh

#delete the keystore
oc delete secret personify-keystore

#create the keystore
oc create secret generic personify-keystore --from-file=keystore.p12=/export/data/cert/keystore.p12 --type=opaque

#mysql volume
oc create -f _01_persistent-volume-mysql.yaml

#logs volume
oc create -f _02_persistent-volume-logs.yaml

./create_apps.sh