package be.personify.iam.model.util;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import be.personify.util.generator.MetaInfo;


/**
 * Class mapping a attribute
 * @author wouter
 *
 */
@Entity
@MetaInfo( group="vault", name="file", description="A file", iconClass="sitemap", 
				sortOrderInGroup=1, showInMenu=false,
				isConcept=false, number=1,
				workflowEnabled = false,
				afterCreateGoToLink = "ticket",
				afterDeleteGoToLink = "ticket",
				tableName = "ticket_file"
)
@Table(name="ticket_file", 
	indexes = {@Index(name = "idx_ticket_id", columnList = "ticket_id")}
)
public class TicketFile extends Persisted {
	
	private static final long serialVersionUID = 8813613998298295748L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the concept", showInSearchResultGrid = false)
	@JsonIgnore
	private long id;
	
	@MetaInfo( name="name", description="the name of the file", searchable=true, showInSearchResultGrid=true, required = true, editable = true, validationPattern = "[-_. A-Za-z0-9]+\\.(pdf|txt|csv|gif|png|jpg|jpeg)")
	private String name;
	
	
	@MetaInfo(searchable=false, showInSearchResultGrid=true, name = "content", description = "content", viewable = false, editable = false)
	@Lob
	private byte[] content;
	
	
	@MetaInfo(searchable=false, showInSearchResultGrid=false, name = "description", description = "description", customRenderer = "text_area|rows:3", viewable = false, required = false)
	@Lob
	@Column(length = 20971520)
	private String description;
	
	@OneToOne
	@MetaInfo( name="ticket", description="the poject of the file", searchable=false, showInSearchResultGrid=false, required = false, viewable = false, creatable = false, editable = false)
	private Ticket ticket;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(content);
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((ticket == null) ? 0 : ticket.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TicketFile other = (TicketFile) obj;
		if (!Arrays.equals(content, other.content))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (ticket == null) {
			if (other.ticket != null)
				return false;
		} else if (!ticket.equals(other.ticket))
			return false;
		return true;
	}


	
	
	

	

	

	


}
