docker login -u personify -p $1

docker push ${info.projectName}/personify-mysql-database:${projectConfiguration.version}

<#list projectConfiguration['modules'] as module>
	<#if module['name'] != '_main' && module['name'] != 'packager' && module['name'] != 'client-api' && module['name'] != 'ui-wicket-library'>
docker push ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
	</#if>
</#list>

