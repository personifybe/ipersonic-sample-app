package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Governance", name="article", description="A article", iconClass="sitemap", 
				sortOrderInGroup=1, showInMenu=false,
				isConcept=false, number=1,
				workflowEnabled = false
)
@Table(name="article", indexes = {
		@Index(name = "idx_article_uniq", columnList = "title,category_id", unique=true)
})
public class Article extends Persisted implements Serializable {
	
	private static final long serialVersionUID = 766337755558404668L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(showInSearchResultGrid=false , name="id", description="the id of the article")
	private long id;
	
	@Column(nullable=false)
	@MetaInfo(name="title",description="the title of the article")
	private String title;
	
	@Column(nullable=false)
	@MetaInfo(name="description",description="the description of the article", showInSearchResultGrid = false)
	private String description;
	
	@MetaInfo(name="category",description="the category of the article", showInSearchResultGridMobile = false)
	@ManyToOne
	private Category category;
	
	@MetaInfo(name="visible",description="the visible of the category")
	private boolean visible;
	
	@MetaInfo(name="content",description="the content of the article", showInSearchResultGrid = false, customRenderer = "text_area|rows:20", searchable = false)
	@Lob
	@Column(length = 20971520)
	private String content;
	
	
	@OneToMany(mappedBy="article", cascade=CascadeType.ALL)
	@ElementCollection(targetClass=ArticleImage.class)
	@MetaInfo( name="images", description="The images to which the article is linked. See articleImage_")
	private List<ArticleImage> images;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<ArticleImage> getImages() {
		return images;
	}

	public void setImages(List<ArticleImage> images) {
		this.images = images;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((category == null) ? 0 : category.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((images == null) ? 0 : images.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Article other = (Article) obj;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (images == null) {
			if (other.images != null)
				return false;
		} else if (!images.equals(other.images))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}
	
	

	
	

	
	

}
