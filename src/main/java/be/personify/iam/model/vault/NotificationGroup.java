package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

import be.personify.iam.model.util.Persisted;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Utility", name="NotificationGroup", description="A notification group determining the sending behaviour",
				workflowEnabled=false,
				iconClass = "comment",
				isConcept = false,
				number=9,
				editable = true,
				deletable = false,
				showDeleteButtonOnSearchResult = false,
				showEditButtonOnSearchResult = false)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Table(name="notification_group", 
	indexes = {
		@Index(name = "idx_name", columnList = "name")
	}
)
public class NotificationGroup extends Persisted implements Serializable {
	
	
	private static final long serialVersionUID = -5782408356499805025L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the connector configuration", searchable=false,showInSearchResultGrid=false)
	private long id;
	
	@MetaInfo(name="code", description="the code")
	private String code;
	
	@MetaInfo(name="name", description="the name")
	private String name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificationGroup other = (NotificationGroup) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
	

	
	
	
	
}
