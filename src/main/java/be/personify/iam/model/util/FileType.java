package be.personify.iam.model.util;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import be.personify.util.generator.MetaInfo;


/**
 * Class mapping a attribute
 * @author wouter
 *
 */
@Entity
@MetaInfo( group="vault", name="filetype", description="A filetype", iconClass="sitemap", 
				sortOrderInGroup=1, showInMenu=false,
				isConcept=false, number=1,
				workflowEnabled = false
)
@Table(name="file_type", 
	indexes = {@Index(name = "idx_filetype_name", columnList = "name")}
)
public class FileType extends Persisted {
	
	private static final long serialVersionUID = -3834015316050718692L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the filetype", showInSearchResultGrid = false)
	@JsonIgnore
	private long id;
	
	@MetaInfo( name="name", description="the name of the filetype", searchable=true, showInSearchResultGrid=true, required = true)
	private String name;
	
	@MetaInfo(searchable=false, showInSearchResultGrid=false, name = "description", description = "description", customRenderer = "text_area|rows:3", viewable = false)
	@Lob
	@Column(length = 20971520)
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FileType other = (FileType) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
	
}
