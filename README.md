# ipersonic-sample-app

project to show the generation of code based on a model

## 1. About
------------------------------------

This project demonstrates the code generation based on a simple model and templates.

The model is located in src/main/java

The templates are located in src/main/resources

The configuration is done via the property file located in src/main/resources


## 2. How To
------------------------------------




>mvn clean install

the generated code is located in target/generated/ipersonic







