package be.personify.iam.model.vault;

public enum DeviceType {
	
	PHONE,
	LAPTOP,
	SERVER,
	TABLET,
	WEARABLE,
	CAR

}
