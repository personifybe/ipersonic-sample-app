#!/bin/bash


<#noparse>

source ./functions.sh

case "$1" in
  "start" )
  	</#noparse>
   	<#list projectConfiguration['modules'] as module>
		<#if module['type']	!= '_main'>
		<#if module['type']	!= 'public'>
		<#if !(module['name']?starts_with("ui")) >
		<#if module['name']	!= 'packager'>
		<#if module['name']	!= 'client-api'>
			<#if module['type']	!= 'backend'>
				nohup bash -c "sleep 40 ; . ./${(module['name'])!"noname"}.sh start" &
			<#else>
				. ./${(module['name'])!"noname"}.sh start
			</#if>
		</#if>
		</#if>
		</#if>
		</#if>
		</#if>
	</#list>
    ;;
    <#noparse>
  "stop" )
    </#noparse>
   	<#list projectConfiguration['modules'] as module>
		<#if module['type']	!= '_main'>
		<#if module['type']	!= 'public'>
		<#if !(module['name']?starts_with("ui")) >
		<#if module['name']	!= 'packager'>
		<#if module['name']	!= 'client-api'>
		. ./${(module['name'])!"noname"}.sh stop
		</#if>
		</#if>
		</#if>
		</#if>
		</#if>
	</#list>
    ;;
    <#noparse>
  "status" )
    </#noparse>
   	<#list projectConfiguration['modules'] as module>
		<#if module['type']	!= '_main'>
		<#if module['type']	!= 'public'>
		<#if !(module['name']?starts_with("ui")) >
		<#if module['name']	!= 'packager'>
		<#if module['name']	!= 'client-api'>
		. ./${(module['name'])!"noname"}.sh status
		</#if>
		</#if>
		</#if>
		</#if>
		</#if>
	</#list>
    ;;
    <#noparse>
  *)
    echo "Usage:</#noparse> ${info.projectName} <#noparse> start|stop|status"
    exit 1
esac
</#noparse>