package be.personify.iam.model.util;

public enum TicketType {
	
	BUG,
	FEATURE,
	IMPROVEMENT

}
