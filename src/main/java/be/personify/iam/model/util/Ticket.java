package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="System", name="ticket", description="A ticket", iconClass="ticket-alt", 
				sortOrderInGroup=10, showInMenu=true,
				sortProperty = "id",
				sortOrder = "descending",
				isConcept=false, number=1,
				workflowEnabled = false,
				showDeleteButtonOnSearchResult = false,
				showEditButtonOnSearchResult = false
)
@Table(name="ticket", indexes = {
		@Index(name = "idx_ticket_uniq", columnList = "title,ticketType", unique=true)
})
public class Ticket extends Persisted implements Serializable {
	
	private static final long serialVersionUID = 6090129627897454003L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(showInSearchResultGrid=false , name="id", description="the id of the article")
	private long id;
	
	@Column(nullable=false)
	@MetaInfo(name="title",description="the title of the article")
	private String title;
	
	@MetaInfo(showInSearchResultGridMobile = false)
	@Enumerated(EnumType.STRING)
	private Component component;
	
	@MetaInfo(showInSearchResultGridMobile = false)
	@Enumerated(EnumType.STRING)
	private TicketType ticketType;

	@MetaInfo(showInSearchResultGridMobile = false)
	@Enumerated(EnumType.STRING)
	private TicketPriority priority;
	

	@Enumerated(EnumType.STRING)
	private TicketStatus ticketStatus;
	
	@MetaInfo(name="content",description="the content of the article", showInSearchResultGrid = false, customRenderer = "text_area|rows:20", searchable = false)
	@Lob
	@Column(length = 20971520)
	private String description;
	
	@OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL)
	@MetaInfo( name="ticketFiles", description="The ticketFiles. See projectFile_", addable = true)
	private List<TicketFile> files;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public TicketPriority getPriority() {
		return priority;
	}

	public void setPriority(TicketPriority priority) {
		this.priority = priority;
	}


	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public List<TicketFile> getFiles() {
		return files;
	}

	public void setFiles(List<TicketFile> files) {
		this.files = files;
	}

	public Component getComponent() {
		return component;
	}

	public void setComponent(Component component) {
		this.component = component;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((component == null) ? 0 : component.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((files == null) ? 0 : files.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((priority == null) ? 0 : priority.hashCode());
		result = prime * result + ((ticketStatus == null) ? 0 : ticketStatus.hashCode());
		result = prime * result + ((ticketType == null) ? 0 : ticketType.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ticket other = (Ticket) obj;
		if (component != other.component)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (files == null) {
			if (other.files != null)
				return false;
		} else if (!files.equals(other.files))
			return false;
		if (id != other.id)
			return false;
		if (priority != other.priority)
			return false;
		if (ticketStatus != other.ticketStatus)
			return false;
		if (ticketType != other.ticketType)
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	
	
	
	
	

}
