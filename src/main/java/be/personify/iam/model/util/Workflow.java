package be.personify.iam.model.util;

public enum Workflow {
	
	APPROVAL_ROLE,
	APPROVAL_ORG_ADMIN,
	APPROVAL_ENT_ADMIN,
	APPROVAL_ATTRIBUTE_MATCHER

}
