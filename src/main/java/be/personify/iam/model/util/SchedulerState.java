package be.personify.iam.model.util;

public enum SchedulerState {
	
	IDLE,
	STARTING,
	RUNNING,

}
