package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.vault.Identity;


@Entity
@MetaInfo( group="vault", frontendGroup="Utility", name="Token", description="A token", 
	number=23,iconClass = "comment-alt")
public class Token extends Persisted  implements Serializable{


	private static final long serialVersionUID = -7334395759507966905L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the token")
	private long id;

	@Enumerated(EnumType.STRING)
	@MetaInfo(name="tokencontext", description="The token context of the token, mobile of email confirmation")
	private TokenContext tokenContext;
	
	@OneToOne
	@MetaInfo(name="identity", description="The identity linked to the token : see identity_")
	private Identity identity;
	
	@MetaInfo(name="token", description="The token as a string")
	private String token;
	
	@MetaInfo(name="validityDate", description="The date until this token is valid")
	private Date validityDate;
	
	@MetaInfo(name="confirmed", description="a boolean indicating if the token has been confirmed")
	private boolean confirmed;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public TokenContext getTokenContext() {
		return tokenContext;
	}

	public void setTokenContext(TokenContext tokenContext) {
		this.tokenContext = tokenContext;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Date getValidityDate() {
		return validityDate;
	}

	public void setValidityDate(Date validityDate) {
		this.validityDate = validityDate;
	}

	public boolean isConfirmed() {
		return confirmed;
	}

	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}
	
	

}
