<#list projectConfiguration['modules'] as module>
    <#if module['type'] == 'backend' || module['name']?starts_with("module") >
rm ${module.name}_grafana_dashboard.json
ln ../../../../../${module.name}/grafana_dashboard.json ${module.name}_grafana_dashboard.json
	</#if>
</#list>

docker image build -f Dockerfile_grafana -t personify/personify-grafana:${projectConfiguration.version} .

