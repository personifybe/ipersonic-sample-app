package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;


@Entity
@MetaInfo( group="vault", frontendGroup="System", name="Translation", description="A translation", showInMenu = true,
			number=25,iconClass = "comment-alt")
@Table(name="translation", indexes = {
		@Index(name = "idx_translation_key_locale", columnList = "kkey,locale", unique=true)
})
public class VaultTranslation extends Persisted  implements Serializable{

	public static final long serialVersionUID = 726554448087105950L;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", showInSearchResultGrid = false, description="the unique id of the translation")
	private long id;

	@MetaInfo(name="locale", description="the locale of the translation")
	private String locale;
	
	@Column(name="kkey")
	@MetaInfo(name="key", description="the key of the translation")
	private String key;
	
	@Column(name="vvalue")
	@MetaInfo(name="value", description="the value of the translation")
	private String value;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VaultTranslation other = (VaultTranslation) obj;
		if (id != other.id)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}
	
	

}
