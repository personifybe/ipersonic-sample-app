package be.personify.iam.model.util;

public enum ConceptState{
	
	NULL,
	ACTIVE,
	REQUESTED,
	INACTIVE,
	LOCKED,
	ERROR
	

}
