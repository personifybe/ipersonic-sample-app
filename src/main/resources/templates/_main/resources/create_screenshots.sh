
mkdir ./img
<#list entities as entity>
<#if entity.haveToInclude(moduleConfiguration['modelPackage']) >
pageres --header="Accept-Language:en,nl;q=0.8,eo;q=0.6,en-US;q=0.4,fr;q=0.2" --filename=./img/frontend_${entity.multiName} [http://localhost:8080/${entity.multiName?lower_case} 800x600] --overwrite
</#if>
</#list>
