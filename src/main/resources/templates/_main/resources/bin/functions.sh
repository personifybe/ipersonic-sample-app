#!/bin/bash
if [[ -n $(type -p java) ]] ; then
    _java=java
    #version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    _java="$JAVA_HOME/bin/java"
    #version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
else
    echo "no JAVA executable found"
    return -1
fi


files=$(find /home/personify/ext -name '*.jar')
extra_jar=''
count=''
for i in $files; do
    extra_jar=$extra_jar$count$i
    count=':'
done
