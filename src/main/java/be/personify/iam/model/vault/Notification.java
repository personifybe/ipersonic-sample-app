package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="System", name="Notification", description="A notification to a certain user ( mail, sms, ...)",
				workflowEnabled=false,
				showInMenu = true,
				iconClass = "comment",
				isConcept = false,
				number=9,
				afterDeleteGoToLink="identity",
				editable = false,
				deletable = false,
				sortProperty = "creationDate",
				sortOrder = "descending",
				persistentPropertiesToIncludeOnSearchResult = "creationDate", 
				showDeleteButtonOnSearchResult = false,
				showEditButtonOnSearchResult = false)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Table(name="notification", 
	indexes = {
		@Index(name = "idx_sent", columnList = "sent")
	}
)
public class Notification extends Persisted implements Serializable {
	
	
	private static final long serialVersionUID = -5782408356499805025L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the connector configuration", searchable=false,showInSearchResultGrid=false)
	private long id;
	
	@Enumerated(EnumType.STRING)
    @NotNull
    @Column(name="notification_type")
	@MetaInfo(name="notificationType", description="the type of the notification (MAIL/SMS)")
	private NotificationType notificationType;
	
	@MetaInfo(name="subject", description="the subject or title of this notification")
	private String subject;
	
	@MetaInfo(searchable=false,showInSearchResultGrid=false, name="bodyContent", description="the full content of the notification", customRenderer = "text_area|rows:10")
	@Lob
	@Column(length = 20971520)
	private String bodyContent;
	
	@OneToOne
	@JoinColumn(name="identity_id", foreignKey=@ForeignKey(name = "FK_notification_identity"))
	@JsonInclude
	@MetaInfo(name="identity", description="the identity_ to which this notification is relevant")
	private Identity identity;
	
	@MetaInfo(name="sent", description="boolean indicating if this notification is sent or not")
	private boolean sent;
	
	@MetaInfo(searchable=false,showInSearchResultGrid=false, name="sendDate", description="the date on which the notification has been sent")
	private Date sendDate;
	
	@Enumerated(EnumType.STRING)
    @Column(name="notification_sent_status")
	@MetaInfo(searchable=false,showInSearchResultGrid=false, name="notificationSentStatus", description = "the status of the sending of the notification (ERROR, SUCCES, .. )")
	private NotificationSentStatus notificationSentStatus;
	
	@MetaInfo(searchable=false,showInSearchResultGrid=false, name="notificiationSentDetail", description = "stirng containing the details of the sending")
	private String notificationSentDetail;
	
	
	@OneToOne
	@JoinColumn(name="notification_id", foreignKey=@ForeignKey(name = "FK_notification_ngroup"))
	@JsonInclude
	@MetaInfo(name="notificationGroup", description="the notificationGroup_ to which this notification is belonging", showInSearchResultGrid = false)
	private NotificationGroup notificationGroup;
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public NotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(NotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getBodyContent() {
		return bodyContent;
	}

	public void setBodyContent(String bodyContent) {
		this.bodyContent = bodyContent;
	}
	
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public NotificationSentStatus getNotificationSentStatus() {
		return notificationSentStatus;
	}

	public void setNotificationSentStatus(NotificationSentStatus notificationSentStatus) {
		this.notificationSentStatus = notificationSentStatus;
	}

	public String getNotificationSentDetail() {
		return notificationSentDetail;
	}

	public void setNotificationSentDetail(String notificationSentDetail) {
		this.notificationSentDetail = notificationSentDetail;
	}

	public NotificationGroup getNotificationGroup() {
		return notificationGroup;
	}

	public void setNotificationGroup(NotificationGroup notificationGroup) {
		this.notificationGroup = notificationGroup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((bodyContent == null) ? 0 : bodyContent.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((notificationGroup == null) ? 0 : notificationGroup.hashCode());
		result = prime * result + ((notificationSentDetail == null) ? 0 : notificationSentDetail.hashCode());
		result = prime * result + ((notificationSentStatus == null) ? 0 : notificationSentStatus.hashCode());
		result = prime * result + ((notificationType == null) ? 0 : notificationType.hashCode());
		result = prime * result + ((sendDate == null) ? 0 : sendDate.hashCode());
		result = prime * result + (sent ? 1231 : 1237);
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Notification other = (Notification) obj;
		if (bodyContent == null) {
			if (other.bodyContent != null)
				return false;
		} else if (!bodyContent.equals(other.bodyContent))
			return false;
		if (id != other.id)
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (notificationGroup == null) {
			if (other.notificationGroup != null)
				return false;
		} else if (!notificationGroup.equals(other.notificationGroup))
			return false;
		if (notificationSentDetail == null) {
			if (other.notificationSentDetail != null)
				return false;
		} else if (!notificationSentDetail.equals(other.notificationSentDetail))
			return false;
		if (notificationSentStatus != other.notificationSentStatus)
			return false;
		if (notificationType != other.notificationType)
			return false;
		if (sendDate == null) {
			if (other.sendDate != null)
				return false;
		} else if (!sendDate.equals(other.sendDate))
			return false;
		if (sent != other.sent)
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}


	
	
	
	
}
