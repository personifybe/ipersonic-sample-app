#!/bin/bash
dockerMode="i"
if [ "$#" -eq  "0" ]
   then
     echo "No docker mode given ( first arg ), defaulting to -$dockerMode"
else
     dockerMode="$1"
     echo "Using docker mode -$dockerMode"
fi


docker run -$dockerMode --rm --network host\
	-p 3000:3000 \
	--name=personify-grafana \
	 personify/personify-grafana:${projectConfiguration.version}
