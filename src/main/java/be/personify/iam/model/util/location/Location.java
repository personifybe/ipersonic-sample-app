package be.personify.iam.model.util.location;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="Location", description="A location", iconClass = "map-marker-alt", number=32)
public class Location extends Persisted implements Serializable{

	public static final long serialVersionUID = -3095329411069616850L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private double lattitude;
	private double longitude;
	private double altitude;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getLattitude() {
		return lattitude;
	}

	public void setLattitude(double lattitude) {
		this.lattitude = lattitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getAltitude() {
		return altitude;
	}

	public void setAltitude(double altitude) {
		this.altitude = altitude;
	}

}
