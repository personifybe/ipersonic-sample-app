package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="Identity", name="ProcessingPurpose", description="A processing purpose", 
		showInMenu=true, 
		iconClass="question", 
		sortOrderInGroup=6, 
		workflowEnabled=false,
		isConcept = false,
		number=6,
		addable=true,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false)
@Table(name="processing_purpose", indexes = {
		@Index(name = "idx_processingpurpose_code", columnList = "code", unique=false),
		@Index(name = "idx_processing_purpose_uniq", columnList = "code,version", unique=true)
})
public class ProcessingPurpose extends Persisted implements Serializable{
	

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the processing purpose", showInSearchResultGrid = false)
	private long id;

	@MetaInfo(name="code", description="The code of the processing purpose", showInSearchResultGrid = true)
	private String code;
	
	@MetaInfo(name="name", description="The name of the processing purpose", showInSearchResultGrid = true)
	private String name;
	
	@MetaInfo(name="description", description="The description of the processing purpose", showInSearchResultGrid = false, searchable = false)
	private String description;
	
	@MetaInfo(name="version", description="The version of the processing purpose", showInSearchResultGrid = true, fixedInitialValue = "1")
	private int version;
	
	@MetaInfo(name="defaultValidityInMonths", description="The defaultValidityInMonths of the processing purpose", showInSearchResultGrid = false, fixedInitialValue = "12", unit = "months")
	private int defaultValidityInMonths;

	

	public ProcessingPurpose() {
		super();
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getVersion() {
		return version;
	}



	public void setVersion(int version) {
		this.version = version;
	}



	public int getDefaultValidityInMonths() {
		return defaultValidityInMonths;
	}



	public void setDefaultValidityInMonths(int defaultValidityInMonths) {
		this.defaultValidityInMonths = defaultValidityInMonths;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + defaultValidityInMonths;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + version;
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProcessingPurpose other = (ProcessingPurpose) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (defaultValidityInMonths != other.defaultValidityInMonths)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (version != other.version)
			return false;
		return true;
	}
	
	
	
	
	

}
