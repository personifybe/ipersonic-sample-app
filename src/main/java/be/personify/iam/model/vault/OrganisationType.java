package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;

@Entity
@MetaInfo( group="vault", frontendGroup="Organisation", name="OrganisationType", 
			description="A organisation type : for being able to group organisations and create prerequisites", 
			showInMenu=true, sortOrderInGroup=2,
			workflowEnabled=false,
					number=12,
			iconClass="arrows-alt",
			isConcept = true)
public class OrganisationType extends Concept implements Serializable {
	
	private static final long serialVersionUID = 973803282155129160L;

	@MetaInfo( searchable=true, showInSearchResultGrid=true, name="code", description = "the code of the organisation type")
	private String code;
	
	@MetaInfo( searchable=true, showInSearchResultGrid=true, name="name", description = "the name of the organisation type")
	private String name;
	
	@MetaInfo( searchable=true, showInSearchResultGrid=false, name="description", description = "the description of the organisation type")
	private String description;
	
	
	public OrganisationType() {
		this.conceptType = ConceptType.OrganisationType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	@Override
	public String toString() {
		return "OrganisationType [code=" + code + ", name=" + name + ", description=" + description + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganisationType other = (OrganisationType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	

}
