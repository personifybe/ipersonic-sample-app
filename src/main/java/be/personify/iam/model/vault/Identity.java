package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;

@Entity
@MetaInfo( group="vault", frontendGroup="Identity", name="Identity", description="A identity", 
		showInMenu=true, 
		iconClass="user", 
		sortOrderInGroup=1, 
		sortProperty = "lastName",
		sortOrder = "ascending",
		workflowEnabled=false,
		isConcept = true,
		entitlementDirection = "organisationAssignments.entitlementAssignments",
		organisationDirection = "organisationAssignments",
		number=1,
		addable=true,
		showDeleteButtonOnSearchResult = false,
		showEditButtonOnSearchResult = false,
		dashboard=true,
		dashboardField = "firstName")
@NamedEntityGraph(name = "Identity.detail", attributeNodes = @NamedAttributeNode("organisationAssignments"))
@Table(name="identity", indexes = {
		@Index(name = "idx_identity_havetosync", columnList = "haveToSync,active", unique=false),
		@Index(name = "idx_identity_email", columnList = "email", unique=true),
		@Index(name = "idx_identity_code", columnList = "code", unique=true),
		@Index(name = "idx_identity_mobilenr", columnList = "mobileNumber", unique=false)
})
public class Identity extends Concept implements Serializable{
	
	private static final long serialVersionUID = -360908527068463645L;

	@MetaInfo( name="Lastname", description="The lastname of the identity", searchable=true )
	private String lastName;
	
	@MetaInfo( name="Firstname", description="The firstname of the identity", searchable=true )
	private String firstName;

	@MetaInfo( name="Email", description="The email of the identity", searchable=true , editable = false, showInSearchResultGrid = false)
	private String email;
	
	@MetaInfo( name="Mobile", description="The mobile number of the identity", searchable=false, showInSearchResultGrid=false, required = false )
	private String mobileNumber;
	
	@MetaInfo( name="code", description="The code of the identity ( could be SSN/RRN )", searchable=false, showInSearchResultGrid=false, editable = false, viewable = false, required = false )
	private String code;
	
	@Convert(converter=be.personify.iam.model.util.PasswordConverter.class)
	@MetaInfo( name="Password", description="The password of the identity", customRenderer = "password", searchable=false, showInSearchResultGrid=false, viewable=false, editable=false)
	private String password;
	
	@MetaInfo( name="Active", description="The active state of the identity", searchable=false, showInSearchResultGrid=true, showInSearchResultGridMobile = false)
	private Boolean active = Boolean.FALSE;
	
	@MetaInfo( name="EmailConfirmed", description="The confirmed state of email of the identity", searchable=false, showInSearchResultGrid=false, addable=false, viewable = false)
	private Boolean confirmedEmail = Boolean.FALSE;
	
	@MetaInfo( name="MobileConfirmed", description="The confirmed state of the mobile number of identity", searchable=false, showInSearchResultGrid=false, addable=false, viewable = false)
	private Boolean confirmedMobile = Boolean.FALSE;
	
	@Enumerated(EnumType.STRING)
	@MetaInfo( name="Gender", description="The gender of the identity", searchable=false, showInSearchResultGrid=false, required = false, viewable = false)
	private Gender gender;
	
	@MetaInfo( name="Locale", description="The locale of the identity", searchable=false, showInSearchResultGrid=false, viewable = false, editable = true )
	private String locale;
	
	@MetaInfo( name="identityType", description="The type of the identity", searchable=false, showInSearchResultGrid=false )
	@ManyToOne
	private IdentityType identityType;
	
	@OneToMany(mappedBy="identity", cascade=CascadeType.ALL)
	@ElementCollection(targetClass=OrganisationAssignment.class)
	@MetaInfo( name="organisationAssignments", description="The organisations to which the identity is linked. See organisationAssignment_", dashboardSubtypeDirection = "organisation.name")
	private List<OrganisationAssignment> organisationAssignments;
	
	@OneToMany(mappedBy="identity")
	@ElementCollection(targetClass=Device.class)
	@MetaInfo( name="devices", description="The devices linked to this identity. See device_" ,searchable=false, showInSearchResultGrid=false, dashboardExclude = true)
	private List<Device> devices;
	
	@OneToMany(mappedBy="identity", cascade=CascadeType.ALL)
	@ElementCollection(targetClass=Consent.class)
	@MetaInfo( name="consents", description="The consents given by this identity. See consent_", addable = false, showDeleteButtonOnSearchResult = false, showEditButtonOnSearchResult = false, dashboardExclude = true)
	private List<Consent> consents;
	
	@OneToMany(mappedBy="identity", cascade=CascadeType.ALL)
	@ElementCollection(targetClass=Notification.class)
	@MetaInfo( name="notifications", description="The notifications sent or to be sent to this identity. See notification_", addable = false, showDeleteButtonOnSearchResult = false, showEditButtonOnSearchResult = false)
	private List<Notification> notifications;
	
	
	@MetaInfo(name="enableMultiFactorAuthentication", description = "enables requesting a second authentication type on login", searchable=false,showInSearchResultGrid=false, viewable = false, editable = true)
	private Boolean enableMultiFactorAuthentication = Boolean.FALSE;
	
	
	//provisioning
	
	@MetaInfo( name="Have to sync", description="Boolean indicating if the concept has to be synced", searchable=false, showInSearchResultGrid=false, viewable = false, editable = false, addable=false,group="provisioning",sortOrderInGroup=1)
	private Boolean haveToSync = Boolean.FALSE;
	
	@MetaInfo(searchable=false,showInSearchResultGrid=false, name="lastLoginDate", description="the date on which the user has logged in", editable = false, creatable = false, required=false)
	private Date lastLoginDate;
	
	
	public Identity() {
		super(ConceptType.Identity);
	}

	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}

	
	public boolean isConfirmedEmail() {
		return confirmedEmail;
	}


	public void setConfirmedEmail(boolean confirmedEmail) {
		this.confirmedEmail = confirmedEmail;
	}


	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}


	public List<OrganisationAssignment> getOrganisationAssignments() {
		return organisationAssignments;
	}

	public void setOrganisationAssignments(List<OrganisationAssignment> assignments) {
		this.organisationAssignments = assignments;
	}



	public boolean isHaveToSync() {
		return haveToSync;
	}

	public void setHaveToSync(boolean haveToSync) {
		this.haveToSync = haveToSync;
	}


	public String getMobileNumber() {
		return mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public boolean isConfirmedMobile() {
		return confirmedMobile;
	}


	public void setConfirmedMobile(boolean confirmedMobile) {
		this.confirmedMobile = confirmedMobile;
	}
	
	public String getLocale() {
		return locale;
	}


	public void setLocale(String locale) {
		this.locale = locale;
	}


	public List<Notification> getNotifications() {
		return notifications;
	}


	public void setNotifications(List<Notification> notifications) {
		this.notifications = notifications;
	}


	public List<Device> getDevices() {
		return devices;
	}


	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}


	public IdentityType getIdentityType() {
		return identityType;
	}


	public void setIdentityType(IdentityType identityType) {
		this.identityType = identityType;
	}


	public boolean isEnableMultiFactorAuthentication() {
		return enableMultiFactorAuthentication;
	}


	public void setEnableMultiFactorAuthentication(boolean enableMultiFactorAuthentication) {
		this.enableMultiFactorAuthentication = enableMultiFactorAuthentication;
	}




	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	public List<Consent> getConsents() {
		return consents;
	}


	public void setConsents(List<Consent> consents) {
		this.consents = consents;
	}
	
	


	public Date getLastLoginDate() {
		return lastLoginDate;
	}


	public void setLastLoginDate(Date lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((confirmedEmail == null) ? 0 : confirmedEmail.hashCode());
		result = prime * result + ((confirmedMobile == null) ? 0 : confirmedMobile.hashCode());
		result = prime * result + ((consents == null) ? 0 : consents.hashCode());
		result = prime * result + ((devices == null) ? 0 : devices.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((enableMultiFactorAuthentication == null) ? 0 : enableMultiFactorAuthentication.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((haveToSync == null) ? 0 : haveToSync.hashCode());
		result = prime * result + ((identityType == null) ? 0 : identityType.hashCode());
		result = prime * result + ((lastLoginDate == null) ? 0 : lastLoginDate.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((mobileNumber == null) ? 0 : mobileNumber.hashCode());
		
		result = prime * result + ((notifications == null) ? 0 : notifications.hashCode());
		result = prime * result + ((organisationAssignments == null) ? 0 : organisationAssignments.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Identity other = (Identity) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (confirmedEmail == null) {
			if (other.confirmedEmail != null)
				return false;
		} else if (!confirmedEmail.equals(other.confirmedEmail))
			return false;
		if (confirmedMobile == null) {
			if (other.confirmedMobile != null)
				return false;
		} else if (!confirmedMobile.equals(other.confirmedMobile))
			return false;
		if (consents == null) {
			if (other.consents != null)
				return false;
		} else if (!consents.equals(other.consents))
			return false;
		if (devices == null) {
			if (other.devices != null)
				return false;
		} else if (!devices.equals(other.devices))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enableMultiFactorAuthentication == null) {
			if (other.enableMultiFactorAuthentication != null)
				return false;
		} else if (!enableMultiFactorAuthentication.equals(other.enableMultiFactorAuthentication))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender != other.gender)
			return false;
		if (haveToSync == null) {
			if (other.haveToSync != null)
				return false;
		} else if (!haveToSync.equals(other.haveToSync))
			return false;
		if (identityType == null) {
			if (other.identityType != null)
				return false;
		} else if (!identityType.equals(other.identityType))
			return false;
		if (lastLoginDate == null) {
			if (other.lastLoginDate != null)
				return false;
		} else if (!lastLoginDate.equals(other.lastLoginDate))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (mobileNumber == null) {
			if (other.mobileNumber != null)
				return false;
		} else if (!mobileNumber.equals(other.mobileNumber))
			return false;
		if (notifications == null) {
			if (other.notifications != null)
				return false;
		} else if (!notifications.equals(other.notifications))
			return false;
		if (organisationAssignments == null) {
			if (other.organisationAssignments != null)
				return false;
		} else if (!organisationAssignments.equals(other.organisationAssignments))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		return true;
	}



	
	
	
	
	
	
	
		

}
