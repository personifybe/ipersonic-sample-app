echo "using tomcat at $TOMCAT_HOME"
<#list projectConfiguration['modules'] as module>
	<#if module['name']?starts_with("ui-") || module['type'] == 'public'>
cp -v ../lib/${projectConfiguration['name']}-${module['name']}-${projectConfiguration['version']}.war $TOMCAT_HOME/webapps/${module['name']}.war
	</#if>
</#list>
