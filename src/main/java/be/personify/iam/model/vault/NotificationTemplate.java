package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="System", name="NotificationTemplate", description="A notification template",
		workflowEnabled=false,
		showInMenu = true,
		iconClass = "edit",
				number=10,
		isConcept = false)
@Table(name="notification_template", indexes = {
		@Index(name = "idx_notification_template_name", columnList = "name"),
		@Index(name = "idx_notification_template_name_loc", columnList = "name,locale", unique=true)
})
public class NotificationTemplate extends Persisted implements Serializable {
	
	
	private static final long serialVersionUID = 1061895926918387602L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="the id of the connector configuration", searchable=false,showInSearchResultGrid=false)
	private long id;
	
	@MetaInfo(name="name", description="the name of the template")	
	private String name;
	
	@MetaInfo(name="subject", description="the subject of the template", showInSearchResultGrid = false)
	private String subject;
	
	@Lob
	@Column(length = 20971520)
	@MetaInfo(searchable=false,showInSearchResultGrid=false, name="templateContent", customRenderer = "text_area|rows:10", description="the content of the template")	
	private String templateContent;
	
	@MetaInfo(name="contentType", description="the content type of the template  ( TEXT_HTML,TEXT_PLAIN,SMS )")	
	@Enumerated(EnumType.STRING)
	private ContentType contentType;
	
	@MetaInfo(name="locale", description="the locale of the template")	
	private String locale;
	
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTemplateContent() {
		return templateContent;
	}

	public void setTemplateContent(String templateContent) {
		this.templateContent = templateContent;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		result = prime * result + ((templateContent == null) ? 0 : templateContent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotificationTemplate other = (NotificationTemplate) obj;
		if (contentType != other.contentType)
			return false;
		if (id != other.id)
			return false;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		if (templateContent == null) {
			if (other.templateContent != null)
				return false;
		} else if (!templateContent.equals(other.templateContent))
			return false;
		return true;
	}

	
	
	
	
	
	
	
	
}
