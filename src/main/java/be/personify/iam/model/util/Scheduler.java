package be.personify.iam.model.util;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="System", showInMenu = true, name="Scheduler", description="A scheduler", 
		iconClass = "clock", number=20,
		sortProperty = "lastTimeRun",
		sortOrder = "descending",
		showDeleteButtonOnSearchResult = false)
@Table(name="scheduler", indexes = {
		@Index(name = "idx_scheduler_name", columnList = "name", unique=true)
})
public class Scheduler extends Persisted implements Serializable{
	
	
	private static final long serialVersionUID = -5782408356499805025L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the scheduler", showInSearchResultGrid = false)
	private long id;
	
	@MetaInfo( searchable=true, showInSearchResultGrid=true, name="name", description = "the name of the scheduler")
	private String name;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="className", description = "the className of the scheduler")
	private String className;
	
	@MetaInfo( searchable=true, showInSearchResultGrid=true, name="active", description = "the active state of the scheduler")
	private boolean active;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="crontab", description = "the crontab of the scheduler http://www.quartz-scheduler.org/documentation/quartz-2.3.0/tutorials/crontrigger.html")
	private String crontab;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="configuration", description = "the configuration of the scheduler", customRenderer = "text_area|rows:10")
	@Lob
	@Column(length = 20971520)
	private String configuration;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="lastTimeRunSuccess", description = "lastTimeRunSuccess of the scheduler", editable = false, creatable = false, viewable = true)
	private Date lastTimeRunSuccess;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="lastTimeRunError", description = "lastTimeRunError of the scheduler", editable = false, creatable = false, viewable = true)
	private Date lastTimeRunError;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="lastError", description = "lastError of the scheduler", editable = false, creatable = false, viewable = true)
	@Lob
	@Column(length = 20971520)
	private String lastError;
	
	@MetaInfo( searchable=false, 
			showInSearchResultGrid=true, 
			showInSearchResultGridMobile = false,
			name="lastTimeRun", description = "lastTimeRun of the scheduler", editable = false, creatable = false)
	private Date lastTimeRun;
	
	@Enumerated
	@MetaInfo( searchable=true, 
			showInSearchResultGrid=false, 
			showInSearchResultGridMobile = false,
			name="state", description = "the state of the scheduler")
	private SchedulerState state;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, viewable = false,
			name="executingInstance", description = "the executingInstance of the scheduler", required = false, editable = false, creatable = false)
	private String executingInstance;
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getCrontab() {
		return crontab;
	}

	public void setCrontab(String crontab) {
		this.crontab = crontab;
	}

	

	public SchedulerState getState() {
		return state;
	}

	public void setState(SchedulerState state) {
		this.state = state;
	}

	public String getExecutingInstance() {
		return executingInstance;
	}

	public void setExecutingInstance(String executingInstance) {
		this.executingInstance = executingInstance;
	}

	public Date getLastTimeRunSuccess() {
		return lastTimeRunSuccess;
	}

	public void setLastTimeRunSuccess(Date lastTimeRunSuccess) {
		this.lastTimeRunSuccess = lastTimeRunSuccess;
	}

	public Date getLastTimeRunError() {
		return lastTimeRunError;
	}

	public void setLastTimeRunError(Date lastTimeRunError) {
		this.lastTimeRunError = lastTimeRunError;
	}

	public Date getLastTimeRun() {
		return lastTimeRun;
	}

	public void setLastTimeRun(Date lastTimeRun) {
		this.lastTimeRun = lastTimeRun;
	}

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((className == null) ? 0 : className.hashCode());
		result = prime * result + ((configuration == null) ? 0 : configuration.hashCode());
		result = prime * result + ((crontab == null) ? 0 : crontab.hashCode());
		result = prime * result + ((executingInstance == null) ? 0 : executingInstance.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((lastError == null) ? 0 : lastError.hashCode());
		result = prime * result + ((lastTimeRun == null) ? 0 : lastTimeRun.hashCode());
		result = prime * result + ((lastTimeRunError == null) ? 0 : lastTimeRunError.hashCode());
		result = prime * result + ((lastTimeRunSuccess == null) ? 0 : lastTimeRunSuccess.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Scheduler other = (Scheduler) obj;
		if (active != other.active)
			return false;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (configuration == null) {
			if (other.configuration != null)
				return false;
		} else if (!configuration.equals(other.configuration))
			return false;
		if (crontab == null) {
			if (other.crontab != null)
				return false;
		} else if (!crontab.equals(other.crontab))
			return false;
		if (executingInstance == null) {
			if (other.executingInstance != null)
				return false;
		} else if (!executingInstance.equals(other.executingInstance))
			return false;
		if (id != other.id)
			return false;
		if (lastError == null) {
			if (other.lastError != null)
				return false;
		} else if (!lastError.equals(other.lastError))
			return false;
		if (lastTimeRun == null) {
			if (other.lastTimeRun != null)
				return false;
		} else if (!lastTimeRun.equals(other.lastTimeRun))
			return false;
		if (lastTimeRunError == null) {
			if (other.lastTimeRunError != null)
				return false;
		} else if (!lastTimeRunError.equals(other.lastTimeRunError))
			return false;
		if (lastTimeRunSuccess == null) {
			if (other.lastTimeRunSuccess != null)
				return false;
		} else if (!lastTimeRunSuccess.equals(other.lastTimeRunSuccess))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (state != other.state)
			return false;
		return true;
	}

	
	
	
	
	
	
	
	
	
	
	

	
}
