package be.personify.iam.model;

public enum ConceptType{
	NULL,
	IdentityType,
	OrganisationType,
	Identity,
	Organisation,
	Entitlement,
	Transformer,
	ProvisioningUnit,
	OrganisationTypeAssignment,
	OrganisationAssignment,
	EntitlementAssignment,
	EntitlementRequest,
	Device,
	EntitlementConflict,

	
}
