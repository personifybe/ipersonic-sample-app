<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'backend' >
CREATE database ${info.projectName?replace('-','_')}_${module.name?replace('-','_')};
CREATE USER '${info.projectName?replace('-','_')}_${module.name?replace('-','_')}'@'%' IDENTIFIED BY 'Azerty1234&';
GRANT usage on *.* to '${info.projectName?replace('-','_')}_${module.name?replace('-','_')}'@'%';
GRANT ALL PRIVILEGES ON *.* TO '${info.projectName?replace('-','_')}_${module.name?replace('-','_')}'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO '${info.projectName?replace('-','_')}_${module.name?replace('-','_')}'@'%' WITH GRANT OPTION;

		</#if>
</#list>

CREATE database mogo_generator_backend_generator;
GRANT usage on *.* to 'mogo_generator_backend_generator'@'%' identified by 'Azerty1234&';
GRANT ALL PRIVILEGES ON *.* TO 'mogo_generator_backend_generator'@'localhost' IDENTIFIED BY 'Azerty1234&' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'mogo_generator_backend_generator'@'%' IDENTIFIED BY 'Azerty1234&' WITH GRANT OPTION;

FLUSH PRIVILEGES;