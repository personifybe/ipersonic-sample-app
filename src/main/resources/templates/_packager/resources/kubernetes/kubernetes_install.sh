printf '\e[38;5;231m'
printf '\e[48;5;64m'
printf '\e[K'

echo "starting installation of personify"


PODS_FOUND=`kubectl get pods -n personify | wc -l`

if [[ $PODS_FOUND != 0 ]] ; then
	read -p "Are you sure you wish to continue, existing pods have been found -> everything will be wiped? yes/no :  "
	if [ "$REPLY" != "yes" ]; then
   		exit
	fi
fi


echo "about 15-20 pods are going to be installed and initialized with a default dataset, please have a little patience, installation would take approximately 5 minutes."

PERCENTAGE=0

#modify /etc/hosts to include the domain

#when using this, use the same password as defined in the gateway module
#./generate_certificate.sh < certificate_input.txt

kubectl delete --ignore-not-found=true -n personify secret personify-keystore | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

kubectl delete --ignore-not-found=true -n personify service personify-mysql-database | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl delete --ignore-not-found=true -n personify deployment personify-mysql-database | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

kubectl delete --ignore-not-found=true -n personify persistentvolumeclaim logs-pv-claim | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl delete --ignore-not-found=true -n personify persistentvolumeclaim ldap-data-claim | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl delete --ignore-not-found=true -n personify persistentvolumeclaim mysql-pv-claim | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl delete --ignore-not-found=true persistentvolume personify-pv-volume | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

PERCENTAGE=1

kubectl create -n personify secret generic personify-keystore --from-file=keystore.p12=keystore.p12 --type=opaque | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

kubectl apply -f _00_namespace.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
PERCENTAGE=2
kubectl apply -f _01_persistent-volume.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl apply -f _01_persistent-volume-claim-mysql.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl apply -f _01_persistent-volume-claim-logs.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
kubectl apply -f _01_persistent-volume-claim-export.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
PERCENTAGE=3
kubectl apply -f _02_app_database.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
PERCENTAGE=4
kubectl expose -n personify deployment personify-mysql-database --port=3306 --name=personify-mysql-database | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

while [[ $(kubectl -n personify get pods -l app=personify-mysql-database -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do sleep 1; done

. ./delete_apps.sh

PERCENTAGE=20

echo "deleted all apps, waiting [20] seconds for cleaning up to arrive" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
sleep 20

. ./create_apps.sh

PERCENTAGE=95

kubectl apply -f 03_gateway_routes.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

ADDRESS=`kubectl describe ingresses.v1.networking.k8s.io personify-gateway -n personify | grep Address | cut -d ':' -f 2 | xargs`
PERCENTAGE=100

echo "-----------------------------------------------------------" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
echo "Installation complete!                                     " | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
echo "Do not forget to add the following line to /etc/hosts!     " | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
echo "$ADDRESS	local.personify.be                               " | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
echo "-----------------------------------------------------------" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
echo "Go to https://local.personify.be                           " | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
echo "-----------------------------------------------------------" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1

