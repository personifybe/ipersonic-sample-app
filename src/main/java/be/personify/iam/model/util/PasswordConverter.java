package be.personify.iam.model.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.springframework.security.crypto.codec.Base64;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

@Converter
public class PasswordConverter implements AttributeConverter<String, String> {
	
	//@Value("${personify.encryption.password:WHERE ARE THE JEWELS!}")
	//TODO tried woth componentscan and all, lost toooo much time on this
	private String password = "WHERE ARE THE JEWELS!";
	
    public static final String SALT = "feccbc02a4a617b0";

    private TextEncryptor cryptor = null;  

    private TextEncryptor getCryptor() {
    	if ( cryptor == null ) {
    		System.out.println("password " + password + " salt " + SALT);
    		cryptor = Encryptors.text(password, SALT);
    	}
    	return cryptor;
    }
    
    
    
    public static void main(String[] args) {
    	PasswordConverter p = new PasswordConverter();
    	System.out.println(p.getCryptor().encrypt("a27d7cde0f9a48c58e628233823804dd"));
	}
    
    
	@Override
	public String convertToDatabaseColumn(String password) {
		if ( password != null ) {
			if(!Base64.isBase64(password.getBytes())) {
				return getCryptor().encrypt(password);
			}
		}
		return password;
	} 

	@Override
	public String convertToEntityAttribute(String dbData) {
		return dbData;
	}
	
	
}
