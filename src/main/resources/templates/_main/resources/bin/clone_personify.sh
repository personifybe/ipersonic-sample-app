ssh-keygen -t rsa -b 4096 -C "personify@personify.be"
cat /home/personify/.ssh/id_rsa.pub
read -p "Press [Enter] key to continue (copy key to git repo first..."
cd
mkdir build
cd build/
git clone git@bitbucket.org:wouter29/personify.git
git clone git@bitbucket.org:wouter29/personify-model.git
git clone git@bitbucket.org:wouter29/personify-scim-server.git
cd personify/generator
mvn clean install
./generate_personify.sh


