package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Organisation", name="OrganisationAssignment", description="A OrganisationAssignment is indicating a link between a identity and a organisation", 
			showInMenu=true, sortOrderInGroup=2,
			workflowEnabled=false,
					number=11,
			iconClass="address-card",
			isConcept = true,
			afterDeleteGoToLink="identity",
			afterCreateGoToLink="identity",
			afterUpdateGoToLink = "identity"
			)
@Table(name="organisation_assignment", indexes = {
		@Index(name = "idx_orgass_uniq", columnList = "identity_id,organisation_id", unique=true)
})
public class OrganisationAssignment extends Concept implements Serializable {

	private static final long serialVersionUID = 7960239031345754584L;

	@ManyToOne( targetEntity=Identity.class)
	@MetaInfo(searchable=false,showInSearchResultGrid=true, editable = false, name="identity", description = "the identity of this organisation assignment", customRenderer = "autoCompleteTextField|search_fields=lastName|display_fields=lastName,firstName,code")
	private Identity identity;
	
	@ManyToOne( targetEntity=Organisation.class)
	@MetaInfo(searchable=true,showInSearchResultGrid=true, editable = false, name="organisation", description = "the organisation of this organisation assignment", customRenderer = "autoCompleteTextField|search_fields=name|display_fields=name,code")
	private Organisation organisation;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=true, showInSearchResultGridMobile = false, name="startDate", description = "the effective start date of the organisation assignement", dateFormat = "dd/MM/yyyy", dateHighlighter = "after.now?date_in_future:date_in_range")
	private Date startDate;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=true, showInSearchResultGridMobile = false, name="endDate", description = "the effective end date of the organisation assignement", dateFormat = "dd/MM/yyyy", dateHighlighter = "after.now?date_in_range:date_expired")
	private Date endDate;
	
	
	@MetaInfo( searchable=true, showInSearchResultGrid=true, name="primaryAssignment", description = "boolean indicating the primary assignment of the organisation assignement", showInSearchResultGridMobile = false)
	private boolean primaryAssignment;
	
	public OrganisationAssignment() {
		this.conceptType = ConceptType.OrganisationAssignment;
	}

	
	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

	public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

	
	public boolean isPrimaryAssignment() {
		return primaryAssignment;
	}

	public void setPrimaryAssignment(boolean primaryAssignment) {
		this.primaryAssignment = primaryAssignment;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getName() {
		if ( identity != null && organisation != null ) {
			return identity.getFirstName() + " " + identity.getLastName() + " for " + organisation.getName();
		}
		return "unknown";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((identity == null) ? 0 : identity.hashCode());
		result = prime * result + ((organisation == null) ? 0 : organisation.hashCode());
		result = prime * result + (primaryAssignment ? 1231 : 1237);
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganisationAssignment other = (OrganisationAssignment) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (identity == null) {
			if (other.identity != null)
				return false;
		} else if (!identity.equals(other.identity))
			return false;
		if (organisation == null) {
			if (other.organisation != null)
				return false;
		} else if (!organisation.equals(other.organisation))
			return false;
		if (primaryAssignment != other.primaryAssignment)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
	
	
	
	
	
}
