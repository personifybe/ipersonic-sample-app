awk -F',' '{printf("{\"name\": %s, \"code\": %s }\n", $1, $2)}' | \
while read s
do
    echo $s
    curl -H "Content-Type: application/json" -H "Authorization: Bearer $1" -X POST -d "$s" http://personify-backend-vault.personify.svc:9190/api/organisation
done