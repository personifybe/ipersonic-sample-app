#!/bin/bash
echo "remember to first start mysql with docker run --name=mysql personify/personify-mysql-database:${projectConfiguration.version}"
dockerMode="i"
if [ "$#" -eq  "0" ]
   then
     echo "No docker mode given ( first arg ), defaulting to -$dockerMode"
else
     dockerMode="$1"
     echo "Using docker mode -$dockerMode"
fi


docker run -$dockerMode --rm \
	-p ${(moduleConfiguration['port'])!"9090"}:${(moduleConfiguration['port'])!"9090"} \
	--name=${moduleName} \
	--link mysql:${info.projectName}-mysql-database.${info.projectName}.svc \
	--link module-authentication:personify-module-authentication.personify.svc \
	${info.projectName}/${info.projectName}-${moduleName}:${projectConfiguration.version}
