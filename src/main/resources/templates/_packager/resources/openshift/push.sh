token=`oc whoami -t`

docker login -u developer -p $token 172.30.1.1:5000

docker tag ${info.projectName}/personify-mysql-database:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/personify-mysql-database:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/personify-mysql-database:${projectConfiguration.version}

#backends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'backend' >
docker tag ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
		</#if>
</#list>

echo "sleeping 40 seconds, waiting for backends to come up"
sleep 40


#modules
<#list projectConfiguration['modules'] as module>
		<#if module['name']?starts_with("module")>
docker tag ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
		</#if>
</#list>

echo "sleeping 60 seconds, waiting for modules to come up"
sleep 60

#public
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'public'>
docker tag ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
		</#if>
</#list>

#documentation
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'documentation'>
docker tag ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
		</#if>
</#list>

echo "sleeping 10 seconds, waiting for public site to come up"
sleep 10

#auth-login
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'authentication-login'>
docker tag ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
		</#if>
</#list>

echo "sleeping 10 seconds, waiting for authentication site to come up"
sleep 10

#frontends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'ui'>
docker tag ${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version} 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
docker push 172.30.1.1:5000/${info.projectName}/${info.projectName}-${module.name}:${projectConfiguration.version}
		</#if>
</#list>

