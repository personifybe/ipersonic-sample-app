package be.personify.iam.model.vault;

public enum ContentType {
	
	TEXT_HTML,
	TEXT_PLAIN,
	SMS;

}
