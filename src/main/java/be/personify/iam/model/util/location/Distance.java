package be.personify.iam.model.util.location;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="Distance", description="A distance", iconClass = "arrow-alt-h", number=31)
public class Distance extends Persisted implements Serializable {
	
	public static final long serialVersionUID = 3481374384619236210L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private int distance;
	
	@Enumerated(EnumType.STRING)
	private DistanceUnit distanceUnit;
	
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public DistanceUnit getDistanceUnit() {
		return distanceUnit;
	}

	public void setDistanceUnit(DistanceUnit distanceUnit) {
		this.distanceUnit = distanceUnit;
	}
	
	

}
