package be.personify.iam.model.util;

public enum WorkflowStatus {
	
	WAITING,
	PROCESSING,
	FINISHED,
	FAILED

}
