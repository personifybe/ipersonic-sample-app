#!/bin/bash

APP_NAME="${info.projectName}-${info.moduleName}-${projectConfiguration.version}"
BASE_DIR=".."
JAR_FILE="$BASE_DIR/lib/$APP_NAME.jar"
PID="/var/tmp/${info.moduleName}.pid"
URL=http://${info.moduleConfiguration['host']}:${info.moduleConfiguration['port']}

source ./functions.sh

<#noparse>

case "$1" in
  "start" )
    if [ ! -f ${PID} ]; then
      echo "Starting ${APP_NAME} ....."
      $_java </#noparse>${projectConfiguration.javaOpts}<#noparse> -Xbootclasspath/a:.:../conf -Dlog4j.configurationFile=../conf/</#noparse>${info.moduleName}_log4j2.xml<#noparse> -jar ${JAR_FILE} &
      echo $! > ${PID}
    else
      echo "${APP_NAME} is already running."
    fi
    ;;
  "stop" )
  	if [ ! -f ${PID} ]; then
  		echo "${PID} not found."
  	else	
    	echo "Stopping ${APP_NAME} ....."
    	kill `cat ${PID}`
	    RETVAL=$?
	    if test $RETVAL -eq 0 ; then
	        echo "succesfully stopped"
	        rm -f ${PID}
	    fi
	    if test $RETVAL -eq 1 ; then
	        echo "could not stop, remove pid file anyway stopped"
	        rm -f ${PID}
	    fi
	fi
    ;;
  "status" )
    response=$(curl --write-out \\n%{http_code} --silent --output - ${URL}/system/health)
    echo </#noparse>${info.moduleName}<#noparse> - ${response}
    ;;
  "log-set" )
    echo "setting level for $2 to $3"
    response=$(curl -v -i -X POST -H 'Content-Type: application/json' -d '{"configuredLevel": "'$3'"}' $URL/system/loggers/$2)
    echo - ${response}
    ;;
  "log-list" )
    response=$(curl -v -i $URL/system/loggers/)
    echo - ${response}
    ;;
  *)
    echo "Usage:</#noparse> ${info.moduleName}.sh <#noparse> start|stop|status|log-list|log-set(logger level)"
    exit 1
esac
</#noparse>