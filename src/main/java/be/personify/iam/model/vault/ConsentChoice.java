package be.personify.iam.model.vault;

public enum ConsentChoice {
	
	OPTIN,
	OPTOUT

}
