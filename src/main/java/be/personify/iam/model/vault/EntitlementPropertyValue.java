package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import be.personify.iam.model.util.Persisted;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Entitlement", name="EntitlementPropertyValue", 
	description="A entitlement property value",
			number=6,
			showDeleteButtonOnSearchResult = false,
			showDetailButtonOnSearchResult = false,
			showEditButtonOnSearchResult = false,
	iconClass= "download")
public class EntitlementPropertyValue extends Persisted implements Serializable{
	

	private static final long serialVersionUID = -587232209364735293L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the object", showInSearchResultGrid = false)
	private long id;
	
	@OneToOne
	@MetaInfo(name="entitlementProperty", description="the entitlementProperty for this entitlementpropertyvalue : see entitlementProperty_", searchable = false, showInSearchResultGrid = true)
	private EntitlementProperty entitlementProperty;
	
	@MetaInfo(name="values", description="The value of the entitlementpropertyvalue")
	@ElementCollection(fetch = FetchType.EAGER)
	@Column(name="_values")
	@CollectionTable(name = "entitlement_prop_values", joinColumns = @JoinColumn(name = "epv_id"))
	private List<String> values = new ArrayList<String>();
	
	
	@OneToOne
	@MetaInfo(name="entitlementAssignment", description="the entitlementAssignment for this entitlementpropertyvalue : see entitlementProperty_", searchable = false, showInSearchResultGrid = false)
	private EntitlementAssignment entitlementAssignment;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<String> getValues() {
		return values;
	}

	public void setValues(List<String> values) {
		this.values = values;
	}

	public EntitlementProperty getEntitlementProperty() {
		return entitlementProperty;
	}

	public void setEntitlementProperty(EntitlementProperty entitlementProperty) {
		this.entitlementProperty = entitlementProperty;
	}
	
	public EntitlementAssignment getEntitlementAssignment() {
		return entitlementAssignment;
	}

	public void setEntitlementAssignment(EntitlementAssignment entitlementAssignment) {
		this.entitlementAssignment = entitlementAssignment;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((entitlementAssignment == null) ? 0 : entitlementAssignment.hashCode());
		result = prime * result + ((entitlementProperty == null) ? 0 : entitlementProperty.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntitlementPropertyValue other = (EntitlementPropertyValue) obj;
		if (entitlementAssignment == null) {
			if (other.entitlementAssignment != null)
				return false;
		} else if (!entitlementAssignment.equals(other.entitlementAssignment))
			return false;
		if (entitlementProperty == null) {
			if (other.entitlementProperty != null)
				return false;
		} else if (!entitlementProperty.equals(other.entitlementProperty))
			return false;
		if (id != other.id)
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}

	
	
	

	
	
	

}
