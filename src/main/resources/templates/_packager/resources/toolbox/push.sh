token=`oc whoami -t`

docker login -u developer -p $token 172.30.1.1:5000

docker tag personify/personify-toolbox:${projectConfiguration.version} 172.30.1.1:5000/personify/personify-toolbox:${projectConfiguration.version}
docker push 172.30.1.1:5000/personify/personify-toolbox:${projectConfiguration.version}
