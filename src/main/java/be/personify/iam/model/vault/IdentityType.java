package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;

@Entity
@MetaInfo( group="vault", frontendGroup="Identity", name="IdentityType", 
			description="A identity type : for being able to group identities", 
			showInMenu=true, sortOrderInGroup=2,
			workflowEnabled=false,
					number=8,
			iconClass="arrows-alt",
			isConcept = true)
public class IdentityType extends Concept implements Serializable {
	
	private static final long serialVersionUID = -9113687709717743068L;


	@MetaInfo(name="code", description="the code of the identitytype")
	private String code;
	
	@MetaInfo(name="name", description="the name of the identitytype")
	private String name;
	
	@MetaInfo(name="description", description="the description of the identitytype", searchable = false, showInMenu = false, showInSearchResultGridMobile = false)
	private String description;
	
	
	public IdentityType() {
		this.conceptType = ConceptType.IdentityType;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	@Override
	public String toString() {
		return "IdentityType [code=" + code + ", name=" + name + ", description=" + description + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdentityType other = (IdentityType) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	
	
	
	
	

}
