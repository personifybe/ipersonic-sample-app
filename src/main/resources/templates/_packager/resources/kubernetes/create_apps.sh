function deploy() {
	MODULE_NAME=$1
	MODULE_PORT=$2
	kubectl apply -f ../../../../../$MODULE_NAME/kube-create-app.yaml | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
	PERCENTAGE=$((PERCENTAGE+3))
	kubectl expose -n personify deployment personify-$MODULE_NAME --port=$MODULE_PORT --name=personify-$MODULE_NAME | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
	while [[ $(kubectl -n personify get pods -l app=personify-$MODULE_NAME -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]]; do sleep 1; done
	PERCENTAGE=$((PERCENTAGE+1))
}



#backends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'backend' >
deploy ${module.name} ${module['configuration']['port']}
		</#if>
</#list>

echo "waiting [40] seconds, waiting for backends to come up" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
sleep 40

#modules
<#list projectConfiguration['modules'] as module>
		<#if module['name']?starts_with("module")>
deploy ${module.name} ${module['configuration']['port']}
echo "waiting [${(module['configuration']['startup-time'])!"10"}] seconds, giving module [${module.name}] time to initialize" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
sleep ${(module['configuration']['startup-time'])!"10"}
		</#if>
</#list>

echo "waiting [30] seconds, waiting for modules to get some breath" | xargs -L 1 echo $PERCENTAGE% `date +'[%Y-%m-%d %H:%M:%S]'` $1
sleep 30

#public
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'public'>
deploy ${module.name} 8080
		</#if>
</#list>


#auth login
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'authentication-login'>
deploy ${module.name} 8080
		</#if>
</#list>

#frontends
<#list projectConfiguration['modules'] as module>
		<#if module['type'] == 'ui' || module['type'] == 'documentation' >
deploy ${module.name} 8080
		</#if>
</#list>

