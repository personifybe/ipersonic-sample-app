package be.personify.iam.model.util;

public enum TicketStatus {
	
	OPEN,
	PENDING_ANALYSIS,
	PENDING_DEVELOPMENT,
	CLOSED,
	REJECTED

}
