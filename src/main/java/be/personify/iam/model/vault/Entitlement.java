package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;

import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Entitlement", name="Entitlement", description="A entitlement", 
		showInMenu=true, 
		number=3,
		identityDirection = "entitlementAssignments.organisationAssignment",
		iconClass="certificate",
		workflowEnabled=false,
		isConcept = true,
		dashboardField="code",
		dashboard = true)
public class Entitlement extends Concept implements Serializable {
		
	
	private static final long serialVersionUID = -8647438972678793837L;
	
	
	public Entitlement() {
		this.conceptType = ConceptType.Entitlement;
	}
	
	@MetaInfo(name="name",description="the name of the entitlement",sampleValue="System Administrator")
	private String name;
	
	@MetaInfo(name="code",description="the code of the entitlement",sampleValue="SYSADMIN", showInSearchResultGrid = false)
	private String code;
	

	@MetaInfo(name="internal",description="the internal attribute of the entitlement",sampleValue="true", showInSearchResultGrid = false)
	private boolean internal;
	
	@MetaInfo( name="conditionPriority", 
			description="the conditionPriority of the entitlement", 
			customRenderer = "integer_dropdown|values:1-20|default:1",
			searchable = false,
			showInSearchResultGrid = false)
	private int conditionPriority;
		
	@MetaInfo( showInSearchResultGrid=false, 
			name="description", 
			description="the description of the entitlement", searchable = false,
			customRenderer = "text_area|rows:3")
	private String description;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=false, 
			name="defaultDurationInMonths", 
			customRenderer = "integer_dropdown|values:1,3,6,12|default:6",
			unit = "months",
			description="the default duration in months, can be used during when creating a entitlementAssignment_")
	private int defaultDurationInMonths;
	
	@MetaInfo( name="requestable", description="Is this entitlement requestable.", showInSearchResultGridMobile = false)
	private boolean requestable;
	
	@MetaInfo( name="assignable", description="Is this entitlement assignable.", showInSearchResultGridMobile = false)
	private boolean assignable;
	
	@MetaInfo( name="riskScore", 
			description="the riskscore of the entitlement", 
			customRenderer = "integer_dropdown|values:1-20|default:1",
			showInSearchResultGrid = false,
			searchable = false)
	private int riskScore;
	
	
	@MetaInfo(name="configuration", description = "the configuration of the entitlement", searchable=false, showInSearchResultGrid=false)
	@ElementCollection(fetch = FetchType.EAGER)
	@JoinTable(name="entitlement_configuration", joinColumns=@JoinColumn(name="id"))
	@MapKeyColumn (name="configuration_id")
	@Column(name="value")
	private Map<String,String> configuration;
	
	
	@OneToMany(cascade = CascadeType.ALL)
	@MetaInfo( name="entitlementProperties", description="The properties linked to this entitlement. See entitlementProperty_", dashboardExclude = true)
	private List<EntitlementProperty> entitlementProperties;
	
	@OneToMany(mappedBy="entitlement", cascade = CascadeType.ALL)
	@MetaInfo( name="entitlementAssignments", description="The assignments linked to this entitlement. See entitlementAssignment_", addable = false, viewable = false, dashboardSubtypeDirection = "organisationAssignment.organisation.name" )
	private List<EntitlementAssignment> entitlementAssignments;
	
		
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDefaultDurationInMonths() {
		return defaultDurationInMonths;
	}

	public void setDefaultDurationInMonths(int defaultDurationInMonths) {
		this.defaultDurationInMonths = defaultDurationInMonths;
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}


	public List<EntitlementProperty> getEntitlementProperties() {
		return entitlementProperties;
	}

	public void setEntitlementProperties(List<EntitlementProperty> entitlementProperties) {
		this.entitlementProperties = entitlementProperties;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<EntitlementAssignment> getEntitlementAssignments() {
		return entitlementAssignments;
	}

	public void setEntitlementAssignments(List<EntitlementAssignment> entitlementAssignments) {
		this.entitlementAssignments = entitlementAssignments;
	}

	public boolean isRequestable() {
		return requestable;
	}

	public void setRequestable(boolean requestable) {
		this.requestable = requestable;
	}

	public int getRiskScore() {
		return riskScore;
	}

	public void setRiskScore(int riskScore) {
		this.riskScore = riskScore;
	}
	
	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}

	public int getConditionPriority() {
		return conditionPriority;
	}

	public void setConditionPriority(int conditionPriority) {
		this.conditionPriority = conditionPriority;
	}


		

	public boolean isAssignable() {
		return assignable;
	}

	public void setAssignable(boolean assignable) {
		this.assignable = assignable;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (assignable ? 1231 : 1237);
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + conditionPriority;
		result = prime * result + ((configuration == null) ? 0 : configuration.hashCode());
		result = prime * result + defaultDurationInMonths;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((entitlementAssignments == null) ? 0 : entitlementAssignments.hashCode());
		result = prime * result + ((entitlementProperties == null) ? 0 : entitlementProperties.hashCode());
		result = prime * result + (internal ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (requestable ? 1231 : 1237);
		result = prime * result + riskScore;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entitlement other = (Entitlement) obj;
		if (assignable != other.assignable)
			return false;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (conditionPriority != other.conditionPriority)
			return false;
		if (configuration == null) {
			if (other.configuration != null)
				return false;
		} else if (!configuration.equals(other.configuration))
			return false;
		if (defaultDurationInMonths != other.defaultDurationInMonths)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (entitlementAssignments == null) {
			if (other.entitlementAssignments != null)
				return false;
		} else if (!entitlementAssignments.equals(other.entitlementAssignments))
			return false;
		if (entitlementProperties == null) {
			if (other.entitlementProperties != null)
				return false;
		} else if (!entitlementProperties.equals(other.entitlementProperties))
			return false;
		if (internal != other.internal)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (requestable != other.requestable)
			return false;
		if (riskScore != other.riskScore)
			return false;
		return true;
	}

	
	
	
	
	
	

	

	
	
	
	
}
