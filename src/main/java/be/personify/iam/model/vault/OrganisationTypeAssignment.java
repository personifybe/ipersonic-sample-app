package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;

@Entity
@MetaInfo( group="vault", frontendGroup="Organisation", name="OrganisationTypeAssignment", 
		description="A link between a organisation and a organisationtype", 
		showInMenu=false,
		iconClass="random",
				number=13,
		workflowEnabled=false,
		isConcept = true,
		afterDeleteGoToLink="organisation",
		afterCreateGoToLink="organisation")
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class OrganisationTypeAssignment extends Concept implements Serializable {
	

	private static final long serialVersionUID = 3948263080483651733L;
	
	@OneToOne
	@JoinColumn(name="organisation_id")
	@JsonInclude
	@MetaInfo( searchable=false, showInSearchResultGrid=false, name="organisation", description = "the organisation_ that is typed")
	private Organisation organisation;
	
	@OneToOne
	@JoinColumn(name="organisation_type_id")
	@JsonInclude
	@MetaInfo( searchable=false, showInSearchResultGrid=true, name="organisationType", description = "the organisation type of the organisation : see organisationType_")
	private OrganisationType organisationType; 
	

	public OrganisationTypeAssignment() {
		this.conceptType = ConceptType.OrganisationTypeAssignment;
	}


	public Organisation getOrganisation() {
		return organisation;
	}


	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}


	public OrganisationType getOrganisationType() {
		return organisationType;
	}


	public void setOrganisationType(OrganisationType organisationType) {
		this.organisationType = organisationType;
	}


	@Override
	public String toString() {
		return "OrganisationTypeAssignment [organisation=" + organisation + ", organisationType=" + organisationType
				+ "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((organisation == null) ? 0 : organisation.hashCode());
		result = prime * result + ((organisationType == null) ? 0 : organisationType.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrganisationTypeAssignment other = (OrganisationTypeAssignment) obj;
		if (organisation == null) {
			if (other.organisation != null)
				return false;
		} else if (!organisation.equals(other.organisation))
			return false;
		if (organisationType == null) {
			if (other.organisationType != null)
				return false;
		} else if (!organisationType.equals(other.organisationType))
			return false;
		return true;
	}
	
	

	
}
