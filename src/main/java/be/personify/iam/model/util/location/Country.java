package be.personify.iam.model.util.location;

import java.io.Serializable;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="Country", description="A country", iconClass = "flag", number=29)
public class Country extends Persisted implements Serializable{
	
	public static final long serialVersionUID = 7434974785310980384L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotNull
	private String code;
	
	private String codeTwoDigit;
	
	private String name;

	@ManyToOne
	@JoinColumn( name="currency_id")
	@MetaInfo(searchable = false)
	private Currency currency;
	
	
	@Size(max=200)
	private String description;
	
	private String capital;
	
	private int numberOfInhabitants;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="location_id")
	@MetaInfo(searchable = false)
	private Location location;
	
	private String telephoneCode;
	
	private String internetExtension;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfInhabitants() {
		return numberOfInhabitants;
	}

	public void setNumberOfInhabitants(int numberOfInhabitants) {
		this.numberOfInhabitants = numberOfInhabitants;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	
	
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getName(Locale locale) {
		Locale obj = new Locale("", code);
		return obj.getDisplayCountry(locale);
	}

	public String getTelephoneCode() {
		return telephoneCode;
	}

	public void setTelephoneCode(String telephoneCode) {
		this.telephoneCode = telephoneCode;
	}

	public String getInternetExtension() {
		return internetExtension;
	}

	public void setInternetExtension(String internetExtension) {
		this.internetExtension = internetExtension;
	}

	public String getCodeTwoDigit() {
		return codeTwoDigit;
	}

	public void setCodeTwoDigit(String codeTwoDigit) {
		this.codeTwoDigit = codeTwoDigit;
	}
	
	
	
	
	

}
