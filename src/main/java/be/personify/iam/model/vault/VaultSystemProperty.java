package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;
import be.personify.iam.model.util.SystemPropertyType;


@Entity
@MetaInfo( group="vault", frontendGroup="System", name="SystemProperty", 
	description="A system property", iconClass = "cog", showInMenu=true,
	number=40
)
@Table(name="system_property", indexes = {
		@Index(name = "idx_system_property_key", columnList = "kkey", unique=true)
})
public class VaultSystemProperty extends Persisted implements Serializable{

	private static final long serialVersionUID = 1797157339855588526L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(showInSearchResultGrid=false , name="id", description="the id of the property")
	private long id;

	@Column(name="kkey")
	@MetaInfo(name="key", description="the key of the property")
	private String key;
	
	@Column(name="vvalue")
	@MetaInfo(name="value", description="the value of the property")
	private String value;
	
	@Enumerated( EnumType.STRING )
	@Column(name="ttype")
	@MetaInfo(showInSearchResultGrid=false , name="type", description="the type of the property")
	private SystemPropertyType type;
	
	@Column(name="ggroup")
	@MetaInfo(showInSearchResultGrid=false , name="group", description="the group of the property")
	private String group;
	
	@MetaInfo(showInSearchResultGrid=false , name="confidential", description="the confidential of the property")
	private boolean confidential;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public SystemPropertyType getType() {
		return type;
	}

	public void setType(SystemPropertyType type) {
		this.type = type;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	

	public boolean isConfidential() {
		return confidential;
	}

	public void setConfidential(boolean confidential) {
		this.confidential = confidential;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (confidential ? 1231 : 1237);
		result = prime * result + ((group == null) ? 0 : group.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VaultSystemProperty other = (VaultSystemProperty) obj;
		if (confidential != other.confidential)
			return false;
		if (group == null) {
			if (other.group != null)
				return false;
		} else if (!group.equals(other.group))
			return false;
		if (id != other.id)
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	
	
	
	

}
