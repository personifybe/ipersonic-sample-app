package be.personify.iam.model.vault;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import be.personify.iam.model.util.Persisted;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Entitlement", name="EntitlementProperty", 
	description="A entitlement property",
			number=5,
			afterCreateGoToLink = "entitlement",
			afterUpdateGoToLink = "entitlement",
			afterDeleteGoToLink = "entitlement",
	iconClass= "download")
public class EntitlementProperty extends Persisted implements Serializable {
	

	private static final long serialVersionUID = 167254423421008961L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(name="id", description="The id of the object", showInSearchResultGrid = false)
	private long id;
	
	@OneToOne
	@MetaInfo(name="entitlement", description="the entitlement for this entitlementproperty : see entitlement_", searchable = false, showInSearchResultGrid = false, editable = false)
	private Entitlement entitlement;
	
	@MetaInfo(name="name", description="The id of the entitlement property")
	private String name;
	
	@MetaInfo(name="description", description="The description of the entitlement property", searchable = false, showInSearchResultGrid = false)
	private String description;
	
	@MetaInfo(name="multiValued", description="indicating the fact the property can have multiple values", searchable = false, showInSearchResultGrid = false)
	private boolean multiValued;
	
	@MetaInfo(name="visible", description="indicating the fact the property is visible or not", searchable = false, showInSearchResultGrid = false)
	private boolean visible;
	
	@MetaInfo(name="minSelected", description="the minimum values for this property", searchable = false, showInSearchResultGrid = false)
	private int minSelected;
	
	@MetaInfo(name="maxSelected", description="the maximum values for this property", searchable = false, showInSearchResultGrid = false)
	private int maxSelected;
	
	
	@MetaInfo(name="componentRendererClass", description="the class used to render this component", searchable = false, showInSearchResultGrid = false, required = false)
	private String componentRendererClass;
	
	
	
	/**
	 * Constructor
	 */
	public EntitlementProperty() {}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isMultiValued() {
		return multiValued;
	}

	public void setMultiValued(boolean multiValued) {
		this.multiValued = multiValued;
	}

	public int getMinSelected() {
		return minSelected;
	}

	public void setMinSelected(int minSelected) {
		this.minSelected = minSelected;
	}

	public int getMaxSelected() {
		return maxSelected;
	}

	public void setMaxSelected(int maxSelected) {
		this.maxSelected = maxSelected;
	}

	public String getComponentRendererClass() {
		return componentRendererClass;
	}

	public void setComponentRendererClass(String componentRendererClass) {
		this.componentRendererClass = componentRendererClass;
	}


	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	
	public Entitlement getEntitlement() {
		return entitlement;
	}


	public void setEntitlement(Entitlement entitlement) {
		this.entitlement = entitlement;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((componentRendererClass == null) ? 0 : componentRendererClass.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((entitlement == null) ? 0 : entitlement.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + maxSelected;
		result = prime * result + minSelected;
		result = prime * result + (multiValued ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntitlementProperty other = (EntitlementProperty) obj;
		if (componentRendererClass == null) {
			if (other.componentRendererClass != null)
				return false;
		} else if (!componentRendererClass.equals(other.componentRendererClass))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (entitlement == null) {
			if (other.entitlement != null)
				return false;
		} else if (!entitlement.equals(other.entitlement))
			return false;
		if (id != other.id)
			return false;
		if (maxSelected != other.maxSelected)
			return false;
		if (minSelected != other.minSelected)
			return false;
		if (multiValued != other.multiValued)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}

	
	
	
	
	

}
