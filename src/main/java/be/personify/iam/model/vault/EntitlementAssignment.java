package be.personify.iam.model.vault;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.ConceptType;
import be.personify.iam.model.util.Concept;

@Entity
@MetaInfo( group="vault", frontendGroup="Entitlement", name="Entitlement Assignment", description="A entitlement assignment is a link ( with temporary constraints ) between a entitlement and a organisation assignment",
				showInMenu=true, sortOrderInGroup=3,
				workflowEnabled=false,
				isConcept = true,
				number=4,
				iconClass = "universal-access",
				identityDirection = "organisationAssignment",
				organisationDirection = "organisationAssignment",
				afterDeleteGoToLink = "organisationAssignment",
				afterCreateGoToLink = "organisationAssignment",
				afterUpdateGoToLink = "organisationAssignment")
@Table(name="entitlement_assignment", indexes = {
		@Index(name = "idx_entass_uniq", columnList = "entitlement_id,organisation_assignment_id", unique=true)
})
public class EntitlementAssignment extends Concept implements Serializable{
	
	
	private static final long serialVersionUID = 7711093103335303501L;
	
	
	@ManyToOne
	@JoinColumn(name="entitlement_id")
	@MetaInfo(name="entitlement", description="the entitlement linked to the assignment : see entitlement_", searchable=true, showInSearchResultGrid=true, editable = false)
	private Entitlement entitlement;
	
	@MetaInfo(name="organisationAssignment", description="the organisationAssignment linked to the assignment : see organisationAssignment_", searchable=false, showInSearchResultGrid=false, editable = false, viewable = false )
	@ManyToOne
	@JoinColumn(name="organisation_assignment_id")
	private OrganisationAssignment organisationAssignment;
	
	@OneToMany(mappedBy = "entitlementAssignment", cascade = CascadeType.ALL)
	@MetaInfo(name="entitlementPropertyValues", description="the class used to render this component : see entitlementPropertyValue_", searchable = false, showInSearchResultGrid = false, viewable = true, editable = false, addable = false )
	private List<EntitlementPropertyValue> entitlementPropertyValues;
		
	@MetaInfo( searchable=false, showInSearchResultGrid=true, showInSearchResultGridMobile = false, name="startDate", description="the date the assignment becomes effective", dateFormat = "dd/MM/yyyy", dateHighlighter = "after.now?date_in_future:date_in_range")
	private Date startDate;
	
	@MetaInfo( searchable=false, showInSearchResultGrid=true, showInSearchResultGridMobile = false, name="endDate",description="the date the assignment expires", dateFormat = "dd/MM/yyyy", dateHighlighter = "after.now?date_in_range:date_expired")
	private Date endDate;
	
	@MetaInfo( searchable=true, showInSearchResultGrid=true, name="active",description="boolean indicating if the assignment is active or not")
	private boolean active;
	
	public EntitlementAssignment() {
		this.conceptType = ConceptType.EntitlementAssignment;
	}

	
	public Entitlement getEntitlement() {
		return entitlement;
	}

	public void setEntitlement(Entitlement entitlement) {
		this.entitlement = entitlement;
	}

	
	public OrganisationAssignment getOrganisationAssignment() {
		return organisationAssignment;
	}

	public void setOrganisationAssignment(OrganisationAssignment organisationAssignment) {
		this.organisationAssignment = organisationAssignment;
	}
	

	public List<EntitlementPropertyValue> getEntitlementPropertyValues() {
		return entitlementPropertyValues;
	}


	public void setEntitlementPropertyValues(List<EntitlementPropertyValue> entitlementPropertyValues) {
		this.entitlementPropertyValues = entitlementPropertyValues;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((entitlement == null) ? 0 : entitlement.hashCode());
		result = prime * result + ((entitlementPropertyValues == null) ? 0 : entitlementPropertyValues.hashCode());
		result = prime * result + ((organisationAssignment == null) ? 0 : organisationAssignment.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntitlementAssignment other = (EntitlementAssignment) obj;
		if (active != other.active)
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (entitlement == null) {
			if (other.entitlement != null)
				return false;
		} else if (!entitlement.equals(other.entitlement))
			return false;
		if (entitlementPropertyValues == null) {
			if (other.entitlementPropertyValues != null)
				return false;
		} else if (!entitlementPropertyValues.equals(other.entitlementPropertyValues))
			return false;
		if (organisationAssignment == null) {
			if (other.organisationAssignment != null)
				return false;
		} else if (!organisationAssignment.equals(other.organisationAssignment))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}


	
	
	
	
	
	

}
