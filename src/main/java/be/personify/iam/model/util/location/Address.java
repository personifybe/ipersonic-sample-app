package be.personify.iam.model.util.location;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import be.personify.iam.model.util.Persisted;
import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="Address", description="A Address", iconClass = "home", number=27)
public class Address extends Persisted implements Serializable {
	
	public static final long serialVersionUID = -1473340476948681443L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	private String street;
	private String streetNumber;
	
	@OneToOne
	@JoinColumn(name="city_id")
	@MetaInfo(searchable = false)
	private City city;
	
	@OneToOne
	@JoinColumn(name="country_id")
	@MetaInfo(searchable = false)
	private Country country;
	
	@OneToOne
	@JoinColumn(name="location_id")
	@MetaInfo(searchable = false)
	private Location location;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}
	
	

}
