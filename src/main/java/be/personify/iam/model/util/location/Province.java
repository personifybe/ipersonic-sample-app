package be.personify.iam.model.util.location;

import java.io.Serializable;
import java.util.Locale;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import be.personify.util.generator.MetaInfo;
import be.personify.iam.model.util.Persisted;

@Entity
@MetaInfo( group="vault", frontendGroup="Location", name="Province", description="A province", iconClass = "university", number=33)
public class Province extends Persisted implements Serializable{

	public static final long serialVersionUID = -1801712636107123670L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotNull
	private String code;
	
	private String name;
	
	@Size(max=200)
	private String description;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="capital_city_id")
	@MetaInfo(searchable = false)
	private City capital;
	
	private int numberOfInhabitants;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfInhabitants() {
		return numberOfInhabitants;
	}

	public void setNumberOfInhabitants(int numberOfInhabitants) {
		this.numberOfInhabitants = numberOfInhabitants;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	

	public City getCapital() {
		return capital;
	}

	public void setCapital(City capital) {
		this.capital = capital;
	}

	public String getName(Locale locale) {
		Locale obj = new Locale("", code);
		return obj.getDisplayCountry(locale);
	}


	

}
