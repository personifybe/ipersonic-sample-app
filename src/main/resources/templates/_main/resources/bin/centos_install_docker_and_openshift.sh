adduser personify
passwd personify
yum -y update
yum -y install epel-release
yum -y install htop
yum -y install git
yum -y install maven
yum -y install mlocate
updatedb
yum -y install certbot
yum -y install geany-plugins-geanygendoc
yum -y install python-pygments
yum -y install yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum -y install docker-ce docker-ce-cli containerd.io
usermod -aG docker root
usermod -aG wheel personify
usermod -aG docker personify
newgrp docker <<EONG
mkdir /etc/docker /etc/containers
tee /etc/containers/registries.conf<<EOF
[registries.insecure]
registries = ['172.30.0.0/16']
EOF
tee /etc/docker/daemon.json<<EOF
{
   "insecure-registries": [
     "172.30.0.0/16"
   ]
}
EOF
EONG
systemctl daemon-reload
systemctl restart docker
systemctl enable docker
echo "net.ipv4.ip_forward = 1" | sudo tee -a /etc/sysctl.conf
sysctl -p

DOCKER_BRIDGE=`docker network inspect -f "{{range .IPAM.Config }}{{ .Subnet }}{{end}}" bridge`
sudo firewall-cmd --permanent --new-zone dockerc
sudo firewall-cmd --permanent --zone dockerc --add-source $DOCKER_BRIDGE
sudo firewall-cmd --permanent --zone dockerc --add-port={80,443,8443}/tcp
sudo firewall-cmd --permanent --zone dockerc --add-port={53,8053}/udp
sudo firewall-cmd --reload

#centos 8
#firewall-cmd --zone=public --add-masquerade --permanent
#firewall-cmd --reload

firewall-cmd --zone=public --add-service=https
firewall-cmd --zone=public --add-masquerade --permanent
firewall-cmd --reload





certbot certonly -d www.personify.be,personify.be,mogo.be,www.mogo.be,ipersonic.be,www.ipersonic.be
cd /etc/letsencrypt/live/www.personify.be/
mkdir -p /export/data/cert/
openssl pkcs12 -export -in fullchain.pem -inkey privkey.pem -out /export/data/cert/keystore.p12 -name tomcat -CAfile chain.pem -caname root


yum -y install centos-release-openshift-origin39
yum -y install origin-clients
oc cluster up --public-hostname=personify.be --host-data-dir=/home/openshift --use-existing-config
oc login -u system:admin
oc adm policy add-scc-to-group anyuid system:authenticated

echo "iptables -D INPUT -p tcp --destination-port 8443 -j DROP" > disable_8443.sh
chmod +x ./disable_8443.sh
echo "iptables -A INPUT -p tcp --destination-port 8443 -j DROP" > enable_8443.sh
chmod +x ./enable_8443.sh





