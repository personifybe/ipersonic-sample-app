<#noparse>

if [[ -z $1 ]]; then
    echo "give the module and lines as argument e.g : ./bashToPersonify.sh personify-module-vault"
    exit -1
fi

POD_NAME=`kubectl get pods -n personify | grep $1 | cut -d ' ' -f 1`

echo $POD_NAME

kubectl exec -n personify --stdin --tty $POD_NAME -- /bin/sh


</#noparse>