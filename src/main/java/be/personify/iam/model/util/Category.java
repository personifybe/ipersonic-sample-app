package be.personify.iam.model.util;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import be.personify.util.generator.MetaInfo;

@Entity
@MetaInfo( group="vault", frontendGroup="Documentation", name="category", description="A category", iconClass="sitemap", 
				sortOrderInGroup=1, showInMenu=false,
				isConcept=false, number=1,
				workflowEnabled = false
)
@Table(name="category", indexes = {
		@Index(name = "idx_category_uniq", columnList = "name", unique=true)
})
public class Category extends Persisted implements Serializable {
	
	private static final long serialVersionUID = 766337755558404668L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@MetaInfo(showInSearchResultGrid=false , name="id", description="the id of the category")
	private long id;
	
	@Column(nullable=false)
	@MetaInfo(name="name",description="the name of the category")
	private String name;
	
	@Column(nullable=false)
	@MetaInfo(name="code",description="the code of the category")
	private String code;
	
	@MetaInfo(name="sortOrder",description="the sortOrder of the category")
	private int sortOrder;
	
	@MetaInfo(name="visible",description="the visible of the category")
	private boolean visible;
	
	
	
	@Column(nullable=false)
	@MetaInfo(name="description",description="the description of the category")
	private String description;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}


	public int getSortOrder() {
		return sortOrder;
	}


	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	

	public boolean isVisible() {
		return visible;
	}


	public void setVisible(boolean visible) {
		this.visible = visible;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + sortOrder;
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (sortOrder != other.sortOrder)
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}


	

	
	

	
	
	

}
