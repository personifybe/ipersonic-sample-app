package be.personify.iam.model.util;

public enum TicketPriority {
	
	LOW,
	MEDIUM,
	HIGH

}
